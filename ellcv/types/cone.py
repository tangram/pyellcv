# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np


def cone_principal_axis(cone):
    """
        cone is a 3x3 matrix
    """
    S, R = np.linalg.eig(cone)
    signs = np.sign(S)
    if 0 in signs:
        print("Error: 0 found in eigen values.")
        return None
    if np.sum(signs) == 3 or np.sum(signs) == -3:
        print("Error: all eigen values have the same sign.")
        
    if signs[1]*signs[2] > 0:
        # first eigen value has a different sign
        index = 0
    elif signs[0] * signs[2] > 0:
        # second eigen value has a different sign
        index = 1
    else:
        # third eigen value has a different sign
        index = 2
    axis = R[:, index]
    axis /= np.linalg.norm(axis) # normalize axis
    axis *= np.sign(axis[2]) # force pointing in positive z
    return axis


def backprojection_cone_from_ellipse(ellipse, K_inv):
    """
        This function compute the 3x3 matrix (Euclidean representation) of
        the backprojection cone.
        For the full cone, we should add its position, but it is always [0, 0, 0],
        as the backprojection cone starts from the camera
        Parameters:
            - ell: ellipse [a, b, x, y, angle]
        Returns:
            - cone as a 3x3 matrix
    """
    axes, angle, center = ellipse.decompose()
    x, y = center
    R = np.array([[np.cos(angle), -np.sin(angle)],
                  [np.sin(angle), np.cos(angle)],
                  [0.0, 0.0]])
    # compute the half-axes normalized size
    a = np.sqrt((K_inv[0, 0] * axes[0] * np.cos(angle))**2 + (K_inv[1, 1] * axes[0] * np.sin(angle))**2)
    b = np.sqrt((K_inv[0, 0] * axes[1] * -np.sin(angle))**2 + (K_inv[1, 1] * axes[1] * np.cos(angle))**2)

    Ec = np.zeros(3, dtype=float)
    Kc = K_inv.dot(np.array([x, y, 1.0]))

    Uc = R[:, 0].reshape((-1, 1))
    Vc = R[:, 1].reshape((-1, 1))
    Nc = np.array([0.0, 0.0, 1.0])
    M = (Uc @ Uc.T) / a**2 + (Vc @ Vc.T) / b**2
    W = Nc.reshape((-1, 1)) / Nc.dot(Kc - Ec)
    P = np.eye(3, dtype=float) - (Kc - Ec).reshape((-1, 1)) @ W.T
    Q = W @ W.T
    B = P.T @ M @ P - Q
    return B
