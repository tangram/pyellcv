# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from ellcv.types import Ellipse
from scipy.spatial.transform.rotation import Rotation as Rot


class Ellipsoid():
    def __init__(self, Q):
        if Q.shape!= (4, 4):
            raise TypeError("Invalid matrix size.")
        if np.sum(np.abs(Q - Q.T)) > 1e-3:
            raise ValueError("Matrix should be symmetric")
        self.Q_ = Q / -Q[3, 3]
        self.axes_ = None
        self.R_ = None
        self.center_ = None

    @property
    def Q(self):
        return self.Q_

    @property
    def center(self):
        return -self.Q_[:3, 3]

    @staticmethod
    def from_dual(Q):
        Q_sym = 0.5 * (Q + Q.T)
        return Ellipsoid(Q_sym)

    @staticmethod
    def from_primal(A):
        try:
            return Ellipsoid(np.linalg.inv(A))
        except:
            print("Impossible to create ellipsoid from A: invalid matrix.")
            raise

    @staticmethod
    def compose(axes, R, center):
        if isinstance(R, list):
            R = Rot.from_euler("xyz", R).as_matrix()
        axes = np.asarray(axes).astype(float)
        center = np.asarray(center).astype(float)
        Q = np.diag((axes**2).tolist() + [-1.0])
        Re_w = np.eye(4, dtype=float)
        Re_w[:3, :3] = R.T
        T_center_inv = np.eye(4, dtype=float)
        T_center_inv[:3, 3] = center
        Q_star = T_center_inv @ Re_w.T @ Q @ Re_w @ T_center_inv.T
        return Ellipsoid(Q_star)
        # same but with the inverse at the end (may be more subject to
        # numerical errors)
        # A = np.diag(np.reciprocal(axes**2).tolist() + [-1.0])
        # T_center = np.eye(4, dtype=float)
        # T_center[:3, 3] = -center
        # Re_w = np.eye(4, dtype=float)
        # Re_w[:3, :3] = R.T
        # transf = Re_w @ T_center
        # A = transf.T @ A @ transf
        # return Ellipsoid.from_primal(A)

    def as_dual(self):
        return self.Q_

    def as_primal(self):
        return np.linalg.inv(self.Q_)

    def decompose(self, no_warnings=False):
        if self.axes_ is not None:
            return np.copy(self.axes_), np.copy(self.R_), np.copy(self.center_)
        self.Q_ /= -self.Q_[3, 3]
        center = -self.Q_[:3, 3]
        T_c = np.eye(4, dtype=float)
        T_c[:3, 3] = -center
        Q_center = T_c @ self.Q_ @ T_c.T
        Q_center = 0.5 * (Q_center + Q_center.T)
        D, R = np.linalg.eig(Q_center[:3, :3])
        if not no_warnings and np.any(D < 0.0):
            print("Warning: negative eigen values (bad ellipsoid)")
        if np.linalg.det(R) < 0:
            R[:, -1] *= -1
        ax = np.sqrt(np.abs(D))
        self.axes_ = ax
        self.R_ = R
        self.center_ = center
        return np.copy(self.axes_), np.copy(self.R_), np.copy(self.center_)


    def translate(self, t):
        T_minus_inv = np.eye(4, dtype=float)
        T_minus_inv[:3, 3] = np.asarray(t)
        Q_translated = T_minus_inv @ self.Q_ @ T_minus_inv.T
        Q_translated = 0.5 * (Q_translated + Q_translated.T)
        return Ellipsoid(Q_translated)

    def scale(self, s):
        """
            Scale the ellipsoid axes
        """
        axes, rot, center = self.decompose()
        return Ellipsoid.compose(axes * s, rot, center)

    def full_scale(self, s):
        """
            Scale the complete ellipsoid from the origin: its shape and position.
        """
        axes, rot, center = self.decompose()
        return Ellipsoid.compose(axes * s, rot, center * s)

    def project(self, P):
        C = P @ self.Q_ @ P.T
        C = 0.5 * (C + C.T)
        return Ellipse.from_dual(C)

    def intrinsic_rotate(self, R):
        """
            Rotate the ellipsoid intrinsically. Same as translate to origin,
            rotate and translate back
        """
        axes_, R_, center_ = self.decompose()
        return Ellipsoid.compose(axes_, R @ R_, center_)

    def transform(self, R, t):
        """
            Apply a rigid transform: rotation + translation.
            It can be used for a chance of coordinate system.
        """
        # Equivalent solutions:
        #   - using the intrinsics parameters
        #   - using the primal form
        #   - using the dual form

        # intrinsics parameters
        # axes_, R_, center_ = self.decompose()
        # center_ = R @ center_ + t
        # R_ = R @ R_
        # return Ellipsoid.compose(axes_, R_, center_)

        # primal form
        # R4 = np.eye(4)
        # R4[:3, :3] = R
        # T_inv = np.eye(4)
        # T_inv[:3, 3] = -t
        # A = self.as_primal()
        # A = T_inv.T @ R4 @ A @ R4.T @ T_inv
        # return Ellipsoid.from_primal(A)

        # dual form
        R4 = np.eye(4)
        R4[:3, :3] = R
        T = np.eye(4)
        T[:3, 3] = t
        return Ellipsoid.from_dual(T @ R4 @ self.Q_ @ R4.T @ T.T)

    def get_euclidean_form(self):
        """
            Get the Euclidean form of the ellipsoid.
            Return A, b, c such that:
                x.t @ A @ x + b x + c = 0
        """
        axes, R, center = self.decompose()
        A = np.diag([1.0/axes[0]**2, 1.0/axes[1]**2, 1.0/axes[2]**2])
        A = R @ A @ R.T
        b = -2 * center.dot(A)
        c = center.dot(A).dot(center)
        return A, b, c

    def to_dict(self):
        """
            Serialize the ellipsoid to a dict.
        """
        axes, R, center = self.decompose()
        dico = {
            "axes": axes.tolist(),
            "R": R.tolist(),
            "center": center.tolist(),
        }
        return dico

    def is_inside(self, x):
        """
            Check if a point is inside the ellipsoid.
        """
        axes, R, center = self.decompose()
        pe = R.T.dot(x - center)
        return np.divide(pe**2, axes**2).sum() < 1.0


    @staticmethod
    def from_dict(dico):
        """
            Create an ellipsoid from a dict.
        """
        return Ellipsoid.compose(dico["axes"], np.asarray(dico["R"]),
                                 dico["center"])
