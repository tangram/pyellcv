# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from scipy.spatial.transform import Rotation as Rot

class Ellipse():
    def __init__(self, C):
        if C.shape!= (3, 3):
            raise TypeError("Invalid matrix size.")
        if np.sum(np.abs(C - C.T)) > 1e-3:
            raise ValueError("Matrix should be symmetric")
        self.C_ = C / -C[2, 2]
        self.axes_ = None
        self.angle_ = None
        self.center_ = None

    @property
    def C(self):
        return self.C_

    @property
    def center(self):
        return -self.C_[:2, 2]

    @staticmethod
    def from_dual(C):
        C_sym = 0.5 * (C + C.T)
        return Ellipse(C_sym)

    @staticmethod
    def from_primal(A):
        try:
            return Ellipse(np.linalg.inv(A))
        except:
            print("Impossible to create ellipse from A: invalid matrix.")
            raise

    @staticmethod
    def compose(axes, angle, center):
        axes = np.asarray(axes).astype(float)
        center = np.asarray(center).astype(float)
        C_star = np.diag((axes**2).tolist() + [-1.0])
        T_center = np.eye(3, dtype=float)
        T_center[:2, 2] = center
        Rw_e = Rot.from_euler("z", angle).as_matrix()
        transf = T_center @ Rw_e
        C_star = transf @ C_star @ transf.T
        return Ellipse(C_star)

    @staticmethod
    def from_bbox(bbox, angle=0):
        w = bbox[2] - bbox[0]
        h = bbox[3] - bbox[1]
        cx = (bbox[2] + bbox[0]) / 2
        cy = (bbox[3] + bbox[1]) / 2
        return Ellipse.compose([w/2, h/2], angle, [cx, cy])

    def as_dual(self):
        return self.C_

    def as_primal(self):
        return np.linalg.inv(self.C_)

    def decompose(self):
        if self.axes_ is not None:
            return np.copy(self.axes_), np.copy(self.angle_), np.copy(self.center_)
        self.C_ /= -self.C_[2, 2]
        center = -self.C_[:2, 2]
        T_c = np.eye(3, dtype=float)
        T_c[:2, 2] = -center
        C_center = T_c @ self.C_ @ T_c.T
        C_center = 0.5 * (C_center + C_center.T)
        D, R = np.linalg.eig(C_center[:2, :2])
        if np.linalg.det(R) < 0:
            R[:, -1] *= -1
        if R[0, 0] < 0.0: # force the first axis to be positive in x
            R *= -1.0
        ax = np.sqrt(np.abs(D))
        angle = np.arctan2(R[1, 0], R[0, 0])
        if ax[0] < ax[1]:
            ax[0], ax[1] = ax[1], ax[0]
            angle += np.pi/2
        self.axes_ = ax
        self.angle_ = angle
        self.center_ = center
        return np.copy(self.axes_), np.copy(self.angle_), np.copy(self.center_)


    def translate(self, t):
        T_minus = np.eye(3, dtype=float)
        T_minus[:2, 2] = -np.asarray(t)
        T_minus_inv = np.linalg.inv(T_minus)
        C_translated = T_minus_inv @ self.C_ @ T_minus_inv.T
        C_translated = 0.5 * (C_translated + C_translated.T)
        return Ellipse(C_translated)

    def scale(self, s):
        """
            Scale the ellipse axes
        """
        axes, angle, center = self.decompose()
        return Ellipse.compose(axes * s, angle, center)

    def rotate(self, a):
        """
            Rotate the ellipse
        """
        axes, angle, center = self.decompose()
        angle += a
        if angle > np.pi: angle -= np.pi
        if angle < -np.pi: angle += np.pi
        return Ellipse.compose(axes, angle, center)

    def full_rotate(self, a):
        """
            Full rotate the ellipse (wrt. the origin)
        """
        axes, angle, center = self.decompose()
        angle += a
        if angle > np.pi: angle -= np.pi
        if angle < -np.pi: angle += np.pi
        R = np.array([[np.cos(a), -np.sin(a)],
                      [np.sin(a), np.cos(a)]])
        return Ellipse.compose(axes, angle, R @ center)

    def full_scale(self, s):
        """
            Scale the complete ellipse from the origin: its shape and position.
        """
        axes, angle, center = self.decompose()
        return Ellipse.compose(axes * s, angle, center * s)

    def as_gaussian(self):
        """
            Get the ellipse as a Gaussian distribution. The axes are interpreted
            as the standart deviation. (this corresponds to a probability to
            contain a point of 39.35%)
            Return:
                - mu: center
                - G: covariance matrix
        """
        axes, angle, center = self.decompose()

        A_dual = np.diag(axes**2)
        R = np.array([[np.cos(angle), -np.sin(angle)],
                      [np.sin(angle), np.cos(angle)]])
        cov = R @ A_dual @ R.T
        return center, cov

    def perspective_transform(self, H_inv):
        """
            Transform with an homography H (inverse of H_inv)
            Params:
                - H_inv 3x3 matrix defining the inverse homography
        """
        A2 = H_inv.T @ self.as_primal() @ H_inv
        A2 = 0.5 * (A2 + A2.T)
        return Ellipse.from_primal(A2)

    def perspective_transform_fast(self, H):
        """
            Transform with an homography H, without using the primal form.
            Thus avoiding inversing the matrix.
            Params:
                - H 3x3 matrix of the homography
        """
        C2 = H @ self.C_ @ H.T
        C2 = 0.5 * (C2 + C2.T)
        return Ellipse.from_dual(C2)

    def to_dict(self):
        """
            Serialize the ellipse to a dict.
        """
        axes, angle, center = self.decompose()
        dico = {
            "axes": axes.tolist(),
            "angle": angle.item(),
            "center": center.tolist(),
        }
        return dico

    @staticmethod
    def from_dict(dico):
        """
            Create an ellipse from a dict.
        """
        return Ellipse.compose(dico["axes"], dico["angle"], dico["center"])
