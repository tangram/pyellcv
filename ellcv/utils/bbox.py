# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from ellcv.types import Ellipse, Ellipsoid
from ellcv.utils import cpp


def bbox_from_ellipse_cpp(ell):
    return cpp.bbox_from_ellipse(np.hstack(ell.decompose()))


def bbox_from_ellipse(ellipse):
    """
        Compute the axis-aligned min-area box encolsing the ellipse.
    """
    axes, angle, center = ellipse.decompose()
    a, b = axes
    x, y = center
    c, s = np.cos(angle), np.sin(angle)

    ax = a*c
    ay = a*s
    bx = -b*s
    by = b*c
    xmax = np.sqrt(ax*ax+bx*bx)
    ymax = np.sqrt(ay*ay+by*by)

    return x - xmax, y - ymax, x + xmax, y + ymax



def ellipse_from_bbox(x1, y1, x2, y2):
    """
        Fit ellipse in a 2D bbox
    """
    a = abs(x2 - x1) / 2
    b = abs(y2 - y1) / 2
    cx = (x1 + x2) / 2
    cy = (y1 + y2) / 2
    return Ellipse.compose(np.array([a, b]), 0.0, np.array([cx, cy]))


def bbox_from_ellipsoid(ell):
    """
        Fit an ellipsoid in a 3D bbox
    """
    axes, R, center = ell.decompose()
    M = R @ np.diag(axes)
    delta = np.linalg.norm(M, axis=1)
    return np.hstack([center - delta, center + delta])


def ellipsoid_from_bbox(bb):
    """
        Compute the axis-aligned min-area box encolsing the ellipsoid.
    """
    x1, y1, z1, x2, y2, z2 = bb
    axes = np.array([x2-x1, y2-y1, z2-z1]) * 0.5
    center = np.array([x2+x1, y2+y1, z2+z1]) * 0.5
    return Ellipsoid.compose(axes, np.eye(3), center)
