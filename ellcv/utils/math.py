# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np


def sym2vec(M):
    """
        Return the lower triangular part of a symetric matrix
    """
    res = []
    N = M.shape[0]
    for i in range(N):
        res.extend(M[i:, i])
    return np.asarray(res)


def vec2sym(v):
    """
        Return a symetric matrix from a lower triangular part.
    """
    if  isinstance(v, list):
        v = np.asarray(v)
    x = 1
    n = 1
    while n < v.size:
        x += 1
        n += x
    M = np.zeros((x, x), dtype=np.float)
    a = 0
    for i in range(x):
        M[i:, i] = v[a:a+(x-i)]
        M[i, i:] = v[a:a+(x-i)]
        a += (x-i)
    return M


def gram_schmidt_orthonormalisation(v1, v2):
    e1 = v1 / np.linalg.norm(v1)
    v3 = np.cross(v1, v2)
    v3n = np.linalg.norm(v3)
    if v3n < 1e-6:
        raise RuntimeError("Error: gram schmidt orthonormalization: v1 and v2 are collinear")
    e3 = v3 / v3n
    v2 = np.cross(v3, v1)
    e2 = v2 / np.linalg.norm(v2)
    return np.vstack((e1, e2, e3)).T
