# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from ellcv.utils import look_at
from ellcv.types import Ellipsoid
from scipy.spatial.transform.rotation import Rotation as Rot

def generate_random_scene(n_ellipsoids, axes_range=[0.1, 2.0], orientation_range=[0.0, 360.0], position_range=[-5.0, 5.0]):
    """ Generate a random synthetic scene with ellipsoids
        - n_ellipsoids: number of ellipsoids
        - axes_range: range of values for each half-axis
        - orientation_range: range of values each euler angle
        - position_range: range of values for the positions (in degrees)
        Returns: list of ellipsoids
    """

    ellipsoids = []
    for _ in range(n_ellipsoids):
        axes = np.random.uniform(*axes_range, size=(3))
        euler_angles = np.random.randint(orientation_range[0], orientation_range[1], 3)
        R = Rot.from_euler("zyx", euler_angles, degrees=True).as_matrix()
        center = np.random.uniform(*position_range, size=(3))
        ell = Ellipsoid.compose(axes, R, center)
        ellipsoids.append(ell)
    return ellipsoids


def generate_random_camera(azimuth_range=[0.0, 360.0], elevation_range=[-80.0, 80.0], dist_range=[10.0, 20.0], target_range=[-0.1, 0.1]):
    """ Generate an random camera looking at a random point
        - azimuth_range, elevation_range, dist_range: camera position in spherical coordinates (in degrees)
        - target_range: range of values for the target position
        Return:
            - orientation
            - position
    """

    d = np.random.uniform(*dist_range)
    az = np.random.uniform(np.deg2rad(azimuth_range[0]), np.deg2rad(azimuth_range[1]))
    elev = np.random.uniform(np.deg2rad(elevation_range[0]), np.deg2rad(elevation_range[1]))
    target = np.random.uniform(*target_range, size=(3))

    pos = np.array([np.cos(az) * np.cos(elev) * d, np.sin(az) * np.cos(elev) * d, np.sin(elev) * d])
    orientation = look_at(pos, target)
    return orientation, pos
