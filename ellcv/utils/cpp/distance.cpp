/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "iou/ee.h"
#include "ellipse.h"
#include "metrics.h"
#include <array>
#include <algorithm>
#include <cmath>


#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

namespace py = pybind11;






double compute_ellipses_iou(const Ellipse& ell0, const Ellipse& ell1)
{
    return compute_iou_toms(ell0, ell1);
}

std::vector<double> bbox_from_ellipse(const Ellipse& ell)
{
    Ellipse_t e1;
    e1.axes.x() = ell[0];
    e1.axes.y() = ell[1];
    e1.angle = ell[2];
    e1.center.x() = ell[3];
    e1.center.y() = ell[4];
    Bbox2 bb = bbox_from_ellipse_(e1);
    std::vector<double> ret(4);
    ret[0] = bb(0, 0);
    ret[1] = bb(0, 1);
    ret[2] = bb(1, 0);
    ret[3] = bb(1, 1);
    return ret;
}

std::vector<double> solve_polynomial(double a, double b, double c, bool force_one_solution=false)
{
    double d = b*b - 4 *a * c;
    if (force_one_solution)
        d = 0.0;
    if (std::abs(d) < 1e-8) {
        double res = -0.5 * b / a;
        return {res, res};
    } else if (d < 0) {
        return {};
    } else {
        double sd = std::sqrt(d);
        return {0.5 * (-b-sd) / a,  0.5 * (-b+sd) / a};
    }
}

Eigen::Matrix3d compose_primal_matrix(const Ellipse& ell)
{
    Eigen::Matrix3d A;
    A << 1.0 / std::pow(ell[0], 2), 0.0, 0.0,
         0.0, 1.0 / std::pow(ell[1], 2), 0.0,
         0.0, 0.0, -1.0;
    Eigen::Matrix3d R;
    R << std::cos(ell[2]), -std::sin(ell[2]), 0.0,
         std::sin(ell[2]), std::cos(ell[2]), 0.0,
         0.0, 0.0, 1.0;
    Eigen::Matrix3d T = Eigen::Matrix3d::Identity();
    T(0, 2) = -ell[3];
    T(1, 2) = -ell[4];
    Eigen::Matrix3d C = T.transpose() * R * A * R.transpose() * T;
    return C;
}

std::vector<Eigen::Vector2d> find_intersection_ellipse_horiz_line(const Ellipse& ell, double y, bool force_tangency=false)
{
    Eigen::Matrix3d C = compose_primal_matrix(ell);
    auto res = solve_polynomial(C(0, 0), 2*C(0, 1)*y + 2*C(0, 2), C(1, 1)*std::pow(y, 2) + 2*C(1, 2)*y + C(2, 2), force_tangency);
    if (res.size() == 0) return {};
    Eigen::Vector2d a(res[0], y);
    Eigen::Vector2d b(res[1], y);
    return {a, b};
}


std::vector<Eigen::Vector2d> find_intersection_ellipse_vert_line(const Ellipse& ell, double x, bool force_tangency=false)
{
    Eigen::Matrix3d C = compose_primal_matrix(ell);
    auto res = solve_polynomial(C(1, 1), 2*C(0, 1)*x + 2*C(1, 2), C(0, 0)*std::pow(x, 2) + 2*C(0, 2)*x + C(2, 2), force_tangency);
    if (res.size() == 0) return {};
    Eigen::Vector2d a(x, res[0]);
    Eigen::Vector2d b(x, res[1]);
    return {a, b};
}

std::vector<Eigen::Vector2d> find_intersection_ellipse_bbox(const Ellipse& ell, const Bbox2 bbox)
{
    std::vector<Eigen::Vector2d> inter;
    auto pts = find_intersection_ellipse_horiz_line(ell, bbox(0, 1));
    for (auto& p : pts) {
        if (p[0] >= bbox(0, 0) && p[0] <= bbox(1, 0))
            inter.push_back(p);
    }

    pts = find_intersection_ellipse_vert_line(ell, bbox(0, 0));
    for (auto& p : pts) {
        if (p[1] >= bbox(0, 1) && p[1] <= bbox(1, 1))
            inter.push_back(p);
    }

    pts = find_intersection_ellipse_horiz_line(ell, bbox(1, 1));
    for (auto& p : pts) {
        if (p[0] >= bbox(0, 0) && p[0] <= bbox(1, 0))
            inter.push_back(p);
    }

    pts = find_intersection_ellipse_vert_line(ell, bbox(1, 0));
    for (auto& p : pts) {
        if (p[1] >= bbox(0, 1) && p[1] <= bbox(1, 1))
            inter.push_back(p);
    }
    return inter;
}

std::tuple<bool, std::vector<double>> find_on_image_bbox(const Ellipse& ell, int width, int height)
{
    auto bb = bbox_from_ellipse(ell);
    if (bb[2] < 0 || bb[3] < 0 || bb[0] >= width || bb[1] >= height)
        return {false, bb};

    Eigen::Vector2d p0 = find_intersection_ellipse_vert_line(ell,  bb[0], true)[0];
    Eigen::Vector2d p1 = find_intersection_ellipse_horiz_line(ell, bb[1], true)[0];
    Eigen::Vector2d p2 = find_intersection_ellipse_vert_line(ell,  bb[2], true)[0];
    Eigen::Vector2d p3 = find_intersection_ellipse_horiz_line(ell, bb[3], true)[0];

    std::vector<Eigen::Vector2d> pts = {p0, p1, p2, p3};

    Bbox2 image_bb;
    image_bb << 0, 0, width-1, height-1;

    auto intersections = find_intersection_ellipse_bbox(ell, image_bb);
    pts.insert(pts.end(), intersections.begin(), intersections.end());
    double xmin = static_cast<double>(width-1);
    double ymin = static_cast<double>(height-1);
    double xmax = 0.0;
    double ymax = 0.0;
    int count = 0;

    for (auto& p : pts)
    {
        if (p[0] < 0 || p[0] >= width || p[1] < 0 || p[1] >= height)
            continue;
        xmin = std::min(xmin, p[0]);
        ymin = std::min(ymin, p[1]);
        xmax = std::max(xmax, p[0]);
        ymax = std::max(ymax, p[1]);
        count++;
    }
    if (count == 0) {
        return {true, {0, 0, width-1, height-1}};
    }
    return {true, {xmin, ymin, xmax, ymax}};
}


std::tuple<double, std::vector<double>, std::vector<std::vector<double>>>
 distance_to_ellipse_contour(const Ellipse& ell1, const Ellipse& ell2, int n_sampling)
{
    Ellipse_t e1;
    e1.axes.x() = ell1[0];
    e1.axes.y() = ell1[1];
    e1.angle = ell1[2];
    e1.center.x() = ell1[3];
    e1.center.y() = ell1[4];
    Ellipse_t e2;
    e2.axes.x() = ell2[0];
    e2.axes.y() = ell2[1];
    e2.angle = ell2[2];
    e2.center.x() = ell2[3];
    e2.center.y() = ell2[4];

    return distance_to_ellipse_contour_(e1, e2, n_sampling);
}


std::tuple<double, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_tangency_error(const Ellipse& ell1, const Ellipse& ell2)
{
    Ellipse_t e1;
    e1.axes.x() = ell1[0];
    e1.axes.y() = ell1[1];
    e1.angle = ell1[2];
    e1.center.x() = ell1[3];
    e1.center.y() = ell1[4];
    Ellipse_t e2;
    e2.axes.x() = ell2[0];
    e2.axes.y() = ell2[1];
    e2.angle = ell2[2];
    e2.center.x() = ell2[3];
    e2.center.y() = ell2[4];

    return ellipses_tangency_error_(e1, e2);
}



std::tuple<double, std::vector<double>, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_sampling_distance(const Ellipse& ell1, const Ellipse& ell2, int N)
{
    Ellipse_t e1;
    e1.axes.x() = ell1[0];
    e1.axes.y() = ell1[1];
    e1.angle = ell1[2];
    e1.center.x() = ell1[3];
    e1.center.y() = ell1[4];
    Ellipse_t e2;
    e2.axes.x() = ell2[0];
    e2.axes.y() = ell2[1];
    e2.angle = ell2[2];
    e2.center.x() = ell2[3];
    e2.center.y() = ell2[4];

    return ellipses_sampling_metric(e1, e2, N);
}

std::tuple<double, std::vector<double>, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_sampling_distance_ell(const Ellipse& ell1, const Ellipse& ell2, int N_az, int N_dist, double sampling_scale)
{
    Ellipse_t e1;
    e1.axes.x() = ell1[0];
    e1.axes.y() = ell1[1];
    e1.angle = ell1[2];
    e1.center.x() = ell1[3];
    e1.center.y() = ell1[4];
    Ellipse_t e2;
    e2.axes.x() = ell2[0];
    e2.axes.y() = ell2[1];
    e2.angle = ell2[2];
    e2.center.x() = ell2[3];
    e2.center.y() = ell2[4];

    return ellipses_sampling_metric_ell(e1, e2, N_az, N_dist, sampling_scale);
}

std::tuple<double, std::vector<double>, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_sampling_distance_ell_uniform(const Ellipse& ell1, const Ellipse& ell2, const std::vector<std::vector<double>>& points)
{
    Ellipse_t e1;
    e1.axes.x() = ell1[0];
    e1.axes.y() = ell1[1];
    e1.angle = ell1[2];
    e1.center.x() = ell1[3];
    e1.center.y() = ell1[4];
    Ellipse_t e2;
    e2.axes.x() = ell2[0];
    e2.axes.y() = ell2[1];
    e2.angle = ell2[2];
    e2.center.x() = ell2[3];
    e2.center.y() = ell2[4];

    return ellipses_sampling_metric_ell_uniform(e1, e2, points);
}

std::tuple<double, std::vector<double>, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_sampling_distance_ell_iou(const Ellipse& ell1, const Ellipse& ell2, int N_az, int N_dist, double sampling_scale)
{
    Ellipse_t e1;
    e1.axes.x() = ell1[0];
    e1.axes.y() = ell1[1];
    e1.angle = ell1[2];
    e1.center.x() = ell1[3];
    e1.center.y() = ell1[4];
    Ellipse_t e2;
    e2.axes.x() = ell2[0];
    e2.axes.y() = ell2[1];
    e2.angle = ell2[2];
    e2.center.x() = ell2[3];
    e2.center.y() = ell2[4];

    return ellipses_sampling_metric_ell_iou(e1, e2, N_az, N_dist, sampling_scale);
}

double ellipses_GW2(const Ellipse& ell1, const Ellipse& ell2)
{
    Ellipse_t e1;
    e1.axes.x() = ell1[0];
    e1.axes.y() = ell1[1];
    e1.angle = ell1[2];
    e1.center.x() = ell1[3];
    e1.center.y() = ell1[4];
    Ellipse_t e2;
    e2.axes.x() = ell2[0];
    e2.axes.y() = ell2[1];
    e2.angle = ell2[2];
    e2.center.x() = ell2[3];
    e2.center.y() = ell2[4];

    return gaussian_wasserstein_2d(e1, e2);
}

double ellipses_Bhattacharyya(const Ellipse& ell1, const Ellipse& ell2)
{
    Ellipse_t e1;
    e1.axes.x() = ell1[0];
    e1.axes.y() = ell1[1];
    e1.angle = ell1[2];
    e1.center.x() = ell1[3];
    e1.center.y() = ell1[4];
    Ellipse_t e2;
    e2.axes.x() = ell2[0];
    e2.axes.y() = ell2[1];
    e2.angle = ell2[2];
    e2.center.x() = ell2[3];
    e2.center.y() = ell2[4];

    return bhattacharyya_distance(e1, e2);
}
std::vector<std::vector<double>> generate_ellipse_sampling_points(const Ellipse& ell1, int count_az, int count_dist, double scale)
{
    Ellipse_t e1;
    e1.axes.x() = ell1[0];
    e1.axes.y() = ell1[1];
    e1.angle = ell1[2];
    e1.center.x() = ell1[3];
    e1.center.y() = ell1[4];

    return generate_sampling_points(e1, count_az, count_dist, scale);
}


double generalized_iou(const Ellipse& ell1, const Ellipse& ell2)
{
    Ellipse_t e1;
    e1.axes.x() = ell1[0];
    e1.axes.y() = ell1[1];
    e1.angle = ell1[2];
    e1.center.x() = ell1[3];
    e1.center.y() = ell1[4];
    Ellipse_t e2;
    e2.axes.x() = ell2[0];
    e2.axes.y() = ell2[1];
    e2.angle = ell2[2];
    e2.center.x() = ell2[3];
    e2.center.y() = ell2[4];

    return generalized_iou_(e1, e2);
}


PYBIND11_MODULE(cpp, m) {
    m.doc() = R"pbdoc(
        utils.cpp
        -----------------------

        .. currentmodule:: utils.cpp

        .. autosummary::
           :toctree: _generate
			utils.cpp
    )pbdoc";

    m.def("compute_ellipses_iou", &compute_ellipses_iou, R"pbdoc(
        Compute the IoU between two ellipses.
    )pbdoc");
    m.def("find_on_image_bbox", &find_on_image_bbox);
    m.def("bbox_from_ellipse", &bbox_from_ellipse);
    m.def("distance_to_ellipse_contour", &distance_to_ellipse_contour);
    m.def("ellipses_tangency_error", &ellipses_tangency_error);
    m.def("ellipses_sampling_distance", &ellipses_sampling_distance);
    m.def("ellipses_sampling_distance_ell", &ellipses_sampling_distance_ell);
    m.def("ellipses_sampling_distance_ell_iou", &ellipses_sampling_distance_ell_iou);
    m.def("ellipses_GW2", &ellipses_GW2);
    m.def("ellipses_Bhattacharyya", &ellipses_Bhattacharyya);
    m.def("generate_ellipse_sampling_points", &generate_ellipse_sampling_points);
    m.def("ellipses_sampling_distance_ell_uniform", &ellipses_sampling_distance_ell_uniform);
    m.def("generalized_iou", &generalized_iou);


#ifdef VERSION_INFO
    m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
    m.attr("__version__") = "dev";
#endif
}
