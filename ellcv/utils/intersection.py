# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

from ellcv.types import Ellipsoid, Ellipse
from ellcv.utils import cpp
from ellcv.utils import bbox_from_ellipse, cpp, bbox_from_ellipse_cpp
from ellcv.visu import draw_ellipse
import numpy as np


def compute_ellipses_iou(ell1, ell2):
    """
        Compute the IoU between two ellipses.
        Args:
            - ell1 (ellcv.types.Ellipse): ellipse 1
            - ell2 (ellcv.types.Ellipse): ellipse 2
        Return: iou
    """
    return cpp.compute_ellipses_iou(np.hstack(ell1.decompose()),
                                    np.hstack(ell2.decompose()))


def ellipses_iou_approx(ell1, ell2, resolution=1.0, return_masks=False):
    """
        resolution in pixels/unit
    """
    a1, r1, c1 = ell1.decompose()
    ell1 = Ellipse.compose(a1 * resolution, r1, c1 * resolution)
    a1, r1, c1 = ell2.decompose()
    ell2 = Ellipse.compose(a1 * resolution, r1, c1 * resolution)

    # ell1 = ell1.scale(resolution)
    # ell2 = ell2.scale(resolution)

    bb1 = bbox_from_ellipse(ell1)
    bb2 = bbox_from_ellipse(ell2)

    bbox = [min(bb1[0], bb2[0]), min(bb1[1], bb2[1]),
            max(bb1[2], bb2[2]), max(bb1[3], bb2[3])]
    bbox = list(map(int, bbox))

    bbox[0] = int(bbox[0])
    bbox[1] = int(bbox[1])
    bbox[0] = int(np.ceil(bbox[0]))
    bbox[1] = int(np.ceil(bbox[1]))

    margin = 5
    bbox[0] -= margin
    bbox[1] -= margin
    bbox[2] += margin
    bbox[3] += margin

    ell1 = ell1.translate([-bbox[0], -bbox[1]])
    ell2 = ell2.translate([-bbox[0], -bbox[1]])

    w = bbox[2] - bbox[0] + 1
    h = bbox[3] - bbox[1] + 1
    mask1 = np.zeros((h, w), dtype=np.uint8)
    mask2 = np.zeros((h, w), dtype=np.uint8)

    draw_ellipse(mask1, ell1, color=1, thickness=-1)
    draw_ellipse(mask2, ell2, color=1, thickness=-1)

    mask1 = mask1.astype(float)
    mask2 = mask2.astype(float)
    inter = np.sum(np.multiply(mask1, mask2))
    union = np.sum(mask1+mask2) - inter
    iou = inter / union

    if return_masks:
        return iou, mask1, mask2
    else:
        return iou

def bbox_area(bb):
    return (bb[2] - bb[0]) * (bb[3] - bb[1])

def bboxes_iou(bb1, bb2):
    inter_w = max(min(bb1[2], bb2[2]) - max(bb1[0], bb2[0]), 0)
    inter_h = max(min(bb1[3], bb2[3]) - max(bb1[1], bb2[1]), 0)
    area_inter = inter_h * inter_w
    return area_inter / (bbox_area(bb1) + bbox_area(bb2) - area_inter)

def bbox3d_volume(bb):
    return (bb[3] - bb[0]) * (bb[4] - bb[1]) * (bb[5] - bb[2])

def bbox3d_iou(bb1, bb2):
    inter_x = max(min(bb1[3], bb2[3]) - max(bb1[0], bb2[0]), 0)
    inter_y = max(min(bb1[4], bb2[4]) - max(bb1[1], bb2[1]), 0)
    inter_z = max(min(bb1[5], bb2[5]) - max(bb1[2], bb2[2]), 0)
    volume_inter = inter_x * inter_y * inter_z
    return volume_inter / (bbox3d_volume(bb1) + bbox3d_volume(bb2) - volume_inter)




def solve_polynomial(a, b, c, force_one_solution=False):
    d = b**2 - 4 * a * c
    if force_one_solution:
        d = 0.0
    if abs(d) < 1e-8:
        return [-b / (2*a), -b / (2*a)]
    elif d < 0:
        return []
    else:
        sd = np.sqrt(d)
        return [(-b-sd) / (2*a), (-b+sd) / (2*a)]

def find_intersection_ellipse_horiz_line(ell, y, force_tangency=False):
    """
        Find the intersection between and ellipse and a horizontal line.
        In case of tangency, the tangent point is duplicated.
    """
    C = ell.as_primal()
    res = solve_polynomial(C[0, 0], 2*C[0, 1]*y + 2*C[0, 2], C[1, 1]*y**2 + 2*C[1, 2]*y + C[2, 2], force_tangency)
    if not len(res): return []
    return [(res[0], y), (res[1], y)]

def find_intersection_ellipse_vert_line(ell, x, force_tangency=False):
    """
        Find the intersection between and ellipse and a vertical line.
        In case of tangency, the tangent point is duplicated.
    """
    C = ell.as_primal()
    res = solve_polynomial(C[1, 1], 2*C[0, 1]*x + 2*C[1, 2], C[0, 0]*x**2 + 2*C[0, 2]*x + C[2, 2], force_tangency)
    if not len(res): return []
    return [(x, res[0]), (x, res[1])]

def find_intersection_ellipse_bbox(ell, bbox):
    """
        Find the intersection between an ellipse and a bounding box.
        Up to 8 solutions. Some points might be duplicated in case of tangency.
    """
    inter = []
    pts = find_intersection_ellipse_horiz_line(ell, bbox[1])
    for p in pts:
        if p[0] >= bbox[0] and p[0] <= bbox[2]:
            inter.append(p)
    pts = find_intersection_ellipse_vert_line(ell, bbox[0])
    for p in pts:
        if p[1] >= bbox[1] and p[1] <= bbox[3]:
            inter.append(p)
    pts = find_intersection_ellipse_horiz_line(ell, bbox[3])
    for p in pts:
        if p[0] >= bbox[0] and p[0] <= bbox[2]:
            inter.append(p)
    pts = find_intersection_ellipse_vert_line(ell, bbox[2])
    for p in pts:
        if p[1] >= bbox[1] and p[1] <= bbox[3]:
            inter.append(p)
    return inter

def find_on_image_bbox(ell, width, height):
    # return bbox_from_ellipse_cpp(ell)
    """
        Find the on-image ellipse bounding box (based on the idea in QuadricSlam).
        Returns:
            status: True if their is an inter(ellipse, image) > 0. False otherwise
            bbox: on-image bounding box
    """

    ## Code from quadricSLAM code but not working
    # (https://github.com/best-of-acrv/gtsam-quadrics/blob/854fe8e4618a20b543e2883cfcc172d81aeef759/gtsam_quadrics/geometry/DualConic.cpp#L99)

    # C = ell.as_primal()
    # C /= C[2, 2]

    # a = C[1, 1] - (C[1, 0] * 2)**2 / (4 * C[0, 0])
    # b = C[2, 1] * 2 - (C[1, 0] * 2 * C[2, 0] * 2) / (2 * C[0, 0])
    # c = C[2, 2] - (C[2, 0] * 2)**2 / (4 * C[0, 0])
    # res = solve_polynomial(a, b, c)
    # if len(res) == 0: print("Warning during computation of exterma points on the ellipse")
    # p1 = [(-C[1, 0] * 2 * res[0] - C[2, 0] * 2) / (2 * C[0, 0]), res[0]]
    # p3 = [(-C[1, 0] * 2 * res[1] - C[2, 0] * 2) / (2 * C[0, 0]), res[1]]

    # a = C[0, 0] - (C[1, 0] * 2)**2 / (4 * C[1, 1])
    # b = C[2, 0] * 2 - (C[1, 0] * 2 * C[2, 1] * 2) / (2 * C[1, 1])
    # c = C[2, 2] - (C[2, 1] * 2)**2 / (4 * C[1, 1])
    # res = solve_polynomial(a, b, c)
    # if len(res) == 0: print("Warning during computation of exterma points on the ellipse")
    # p0 = [res[0], (-C[1, 0] * 2 * res[0] - C[2, 1] * 2) / (2 * C[1, 1])]
    # p2 = [res[1], (-C[1, 0] * 2 * res[1] - C[2, 1] * 2) / (2 * C[1, 1])]

    # pts = [p0, p1, p2, p3]

    # Instead, find extrema values in x and y
    # and then find the corresponding point on the ellipse
    # just by find intersection with lines
    xmin, ymin, xmax, ymax = bbox_from_ellipse(ell)
    # if ellipse is completely outside the image return bbox and status = false
    if xmax < 0 or ymax < 0 or xmin >= width or ymin >= height:
        return False, [xmin, ymin, xmax, ymax]

    # find_intersection_* returns two solutions in the case of tangency (the solution is just duplicated)
    p0 = find_intersection_ellipse_vert_line(ell, xmin, force_tangency=True)[0]
    p1 = find_intersection_ellipse_horiz_line(ell, ymin, force_tangency=True)[0]
    p2 = find_intersection_ellipse_vert_line(ell, xmax, force_tangency=True)[0]
    p3 = find_intersection_ellipse_horiz_line(ell, ymax, force_tangency=True)[0]
    pts = [p0, p1, p2, p3]

    image_bb = [0, 0, width-1, height-1]
    pts.extend(find_intersection_ellipse_bbox(ell, image_bb))

    good_pts = []
    for p in pts:
        if p[0] < 0 or p[0] >= width or p[1] < 0 or p[1] >= height:
            continue
        good_pts.append(p)

    # image contained inside the ellipse
    if len(good_pts) == 0:
        return True, image_bb

    good_pts = np.array(good_pts)
    xmin, ymin = np.min(good_pts, axis=0)
    xmax, ymax = np.max(good_pts, axis=0)
    return True, [xmin, ymin, xmax, ymax]


# Wraping for cpp version
def find_on_image_bbox_cpp(ell, width, height):
    return cpp.find_on_image_bbox(np.hstack(ell.decompose()), width, height)
