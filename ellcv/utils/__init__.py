# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

from .math import sym2vec, vec2sym, gram_schmidt_orthonormalisation
from .bbox import bbox_from_ellipse, ellipse_from_bbox, bbox_from_ellipse_cpp, bbox_from_ellipsoid, ellipsoid_from_bbox
from .intersection import compute_ellipses_iou, ellipses_iou_approx, bboxes_iou, bbox_area, \
     find_intersection_ellipse_horiz_line, find_intersection_ellipse_vert_line,find_intersection_ellipse_bbox, \
     find_on_image_bbox, find_on_image_bbox_cpp, bbox3d_iou, bbox3d_volume
from .camera import look_at, pose_error, rotation_error, camera_roll, generate_K
from .scene import generate_random_camera, generate_random_scene
from .distance import tangency_error, Bhattacharyya_distance, ellipses_tangency_error_cpp,\
    bbox_from_ellipse_cpp, distance_to_ellipse_contour_cpp, ellipses_sampling_distance_cpp, \
    ellipses_sampling_distance_ell_cpp, ellipses_sampling_distance_ell_iou_cpp, \
    ellipses_GW2_cpp, ellipses_Bhattacharyya_cpp, generate_ellipse_sampling_points_cpp, \
    ellipses_sampling_distance_ell_uniform_cpp, ellipses_generalized_iou
