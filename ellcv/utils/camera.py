# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from scipy.spatial.transform.rotation import Rotation as Rot

def look_at(center, target, up=np.array([0.0, 0.0, 1.0])):
    z = target - center
    zn = z / np.linalg.norm(z)
    x = np.cross(-up, z)
    if np.linalg.norm(z) < 1e-3:
        raise ValueError("View direction is collinear with up vector")
    xn = x / np.linalg.norm(x)
    y = np.cross(z, x)
    yn = y / np.linalg.norm(y)
    return np.vstack((xn, yn, zn)).T


def rotation_error(R1, R2):
    """
        Returns the rotation error (in radians)
    """
    diff = Rot.from_matrix(R1 @ R2.T).as_rotvec()
    return np.linalg.norm(diff)


def pose_error(cam1, cam2):
    """
        cam1: (orientation, position)
        cam2: (orientation, position)
        Returns:
            rotation error, position error
    """
    pos_error = np.sqrt(np.sum((cam1[1] - cam2[1])**2))
    rot_error = rotation_error(cam1[0], cam2[0])
    return rot_error, pos_error


def camera_roll(R):
    """
        Estimate the roll of a camera (in radians).
        This is the angle deviation from having x horizontal and -y "vertical".
    """
    r0 = np.arctan2(R[2, 1], R[2, 0]) # solve the derivate of z(roll) = 0
    r1 = r0 + np.pi
    if r1 > np.pi: r1 -= np.pi * 2
    # choose the maximum
    if R[2, 0] * np.cos(r0) + R[2, 1] * np.sin(r0) > R[2, 0] * np.cos(r1) + R[2, 1] * np.sin(r1):
        r0 = r1
    r0 -= np.pi/2
    if r0 < -np.pi:
        r0 += np.pi * 2
    if r0 > np.pi:
        r0 -= np.pi * 2
    return -r0


def generate_K(fx, ppx, ppy, fy=None):
    """
        Generat the calibration of a pinhole camera
    """
    if fy is None:
        fy = fx
    return np.array([[fx, 0.0, ppx],
                     [0.0, fy, ppy],
                     [0.0, 0.0, 1.0]])
