# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

from ellcv.types import Ellipse
from ellcv.visu import sample_ellipse_points_params
import numpy as np

from ellcv.utils import cpp


def ellipses_generalized_iou(ell1, ell2):
    return cpp.generalized_iou(np.hstack(ell1.decompose()), np.hstack(ell2.decompose()))


def ellipses_tangency_error_cpp(ell1, ell2):
    return cpp.ellipses_tangency_error(np.hstack(ell1.decompose()), np.hstack(ell2.decompose()))

def ellipses_GW2_cpp(ell1, ell2):
    return cpp.ellipses_GW2(np.hstack(ell1.decompose()), np.hstack(ell2.decompose()))

def ellipses_Bhattacharyya_cpp(ell1, ell2):
    return cpp.ellipses_Bhattacharyya(np.hstack(ell1.decompose()), np.hstack(ell2.decompose()))

def bbox_from_ellipse_cpp(ell):
    return cpp.bbox_from_ellipse(np.hstack(ell.decompose()))

def distance_to_ellipse_contour_cpp(ell1, ell2, n_sampling):
    return cpp.distance_to_ellipse_contour(np.hstack(ell1.decompose()),
                                           np.hstack(ell2.decompose()),
                                           n_sampling)

def ellipses_sampling_distance_cpp(ell1, ell2, N):
    return cpp.ellipses_sampling_distance(np.hstack(ell1.decompose()),
                                          np.hstack(ell2.decompose()),
                                          N)
def ellipses_sampling_distance_ell_cpp(ell1, ell2, N_az, N_dist, sampling_scale):
    return cpp.ellipses_sampling_distance_ell(np.hstack(ell1.decompose()),
                                              np.hstack(ell2.decompose()),
                                              N_az, N_dist, sampling_scale)

def ellipses_sampling_distance_ell_uniform_cpp(ell1, ell2, points):
    return cpp.ellipses_sampling_distance_ell_uniform(np.hstack(ell1.decompose()),
                                                      np.hstack(ell2.decompose()),
                                                      points)

def ellipses_sampling_distance_ell_iou_cpp(ell1, ell2, N_az, N_dist, sampling_scale):
    return cpp.ellipses_sampling_distance_ell_iou(np.hstack(ell1.decompose()),
                                                  np.hstack(ell2.decompose()),
                                                  N_az, N_dist, sampling_scale)

def generate_ellipse_sampling_points_cpp(ell, N_az, N_dist, sampling_scale):
    return cpp.generate_ellipse_sampling_points(np.hstack(ell.decompose()),
                                                N_az, N_dist, sampling_scale)


def tangency_error(ell0, ell1, method="max", sampling=360):
    """
        Compute the tangency error between two ellipses. The ellipse axes are
        # matched by length and the direction with minimal distance is used.
        Parameters:
            - ell0: first ellipse
            - ell1: second ellipse
            - method:
                * "max" for the max distance over the sample points
                * "mean" for the mean distance over the sampled points
            - sampling: number of sampled points
    """
    axes0, angle0, center0 = ell0.decompose()
    axes1, angle1, center1 = ell1.decompose()

    # Match the axes by lengths (largest to largest and smallest to smallest)
    if (axes0[0] > axes0[1] and axes1[0] < axes1[1] or
        axes0[0] < axes0[1] and axes1[0] > axes1[1]):
        axes1[0], axes1[1] = axes1[1], axes1[0]
        angle1 += np.pi/2

    # Check the direction of the first axis (two possibilities) and rotate ell1
    # by 180 deg if needed.
    # We do this checking with the centered ellipses because we want to find
    # the best match for their shape. Using the ellipses with their centers
    # could change the result.
    p00 = np.array([np.cos(angle0) * axes0[0], np.sin(angle0) * axes0[1]])
    p10 = np.array([np.cos(angle1) * axes1[0], np.sin(angle1) * axes1[1]])
    p11 = np.array([np.cos(angle1+np.pi) * axes1[0], np.sin(angle1+np.pi) * axes1[1]])
    if np.sum((p10-p00)**2) > np.sum((p11-p00)**2):
        angle1 += np.pi


    pts0 = sample_ellipse_points_params(axes0, angle0, center0, sampling=sampling)
    pts1 = sample_ellipse_points_params(axes1, angle1, center1, sampling=sampling)

    sq_distances = np.sum((pts0-pts1)**2, axis=1)
    if method == "max":
        result = np.sqrt(np.max(sq_distances))
    elif method == "mean":
        result = np.mean(np.sqrt(sq_distances))
    elif method == "sum":
        result = np.sum(np.sqrt(sq_distances))
    elif method == "residuals":
        result = pts0.flatten()-pts1.flatten()
    else:
        print("Error: unknown method ", method)
        return -1
    return result

def Bhattacharyya_distance(ell0, ell1):
    """
        Compute the Bhattacharyya distance between two ellipses. The ellipses
        interpreted as 2D Gaussian distributions with a standard deviation
        equal to the axes length.
    """
    mu0, G0 = ell0.as_gaussian()
    mu1, G1 = ell1.as_gaussian()
    G = (G0 + G1) / 2
    G_inv = np.linalg.inv(G)
    mu_diff = mu0 - mu1
    dist = (1/8) * mu_diff.dot(G_inv.dot(mu_diff))
    dist += 0.5 * np.log(np.linalg.det(G) / np.sqrt(np.linalg.det(G0) * np.linalg.det(G1)))
    return dist
