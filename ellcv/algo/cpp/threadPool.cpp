/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "threadPool.h"


ThreadPool::ThreadPool(size_t nbThreads) : stop(false)
{
  if (nbThreads == 0)
  {
    nbThreads = std::thread::hardware_concurrency();
  }

  auto workerInternalFunction = [this]() {
    while(true)
    {
      std::function<void()> task;
      {
        std::unique_lock<std::mutex> lock(this->queue_mutex);
        this->condition.wait(lock, [this] {
          // the thread stops waiting if stop or if tasks is not empty
          return this->stop || !this->tasks.empty();
        });
        // if stop or no more tasks return
        if (this->stop || this->tasks.empty())
          return ;
        // get next task
        task = std::move(this->tasks.front());
        this->tasks.pop();
      } // implicit lock.unlock();

      // run next task
      task();
    }
  };

  for (size_t i = 0; i < nbThreads; ++i)
  {
    // fill workers with threads executing the same function
    workers.emplace_back(workerInternalFunction);
  }
}

ThreadPool::~ThreadPool()
{
  stop = true;
  condition.notify_all();

  for (auto& worker : workers)
    worker.join();
}
