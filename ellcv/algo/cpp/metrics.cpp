#include "metrics.h"

#include <iomanip>
#include <iostream>
#include <fstream>

#include <unsupported/Eigen/MatrixFunctions>

#include "ellipse.h"
#include "iou/ee.h"




std::vector<std::vector<double>> generate_sampling_points(const Ellipse_t& ell, int count_az, int count_dist, double scale)
{
    Eigen::Matrix2d R;
    R << std::cos(ell.angle), -std::sin(ell.angle),
         std::sin(ell.angle),  std::cos(ell.angle);

    double d_dist = scale / count_dist;
    double d_az = 2 * M_PI / count_az;
    std::vector<std::vector<double>> points(count_az * count_dist + 1, std::vector<double>(2, 0.0));
    points[0][0] = ell.center.x();
    points[0][1] = ell.center.y();
    for (int i = 0; i < count_dist; ++i)
    {
        for (int j = 0; j < count_az; ++j)
        {
            double s = (i+1) * d_dist;
            Eigen::Vector2d v(std::cos(d_az * j) * ell.axes.x() * s, std::sin(d_az * j) * ell.axes.y() * s);
            Eigen::Vector2d pt = ell.center + R * v;
            points[1 + i * count_az + j][0] = pt.x();
            points[1 + i * count_az + j][1] = pt.y();
        }
    }

    return points;
}



std::tuple<double, std::vector<double>, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_sampling_metric_ell(const Ellipse_t& ell1, const Ellipse_t& ell2, int N_az, int N_dist, double sampling_scale)
{
    // ell1: detection
    // ell2: reprojection

    // Bbox2 bb1 = bbox_from_ellipse_(ell1);
    // double scale = 10.0 / std::max(bb1(1, 0) - bb1(0, 0), bb1(1, 1) - bb1(0, 1));
    // double scale = 1.0 / std::max(ell1.axes[0], ell1.axes[1]);
    // double scale = 1.0;// / std::max(ell1.axes[0], ell1.axes[1]);

    // Bbox2 bb2 = bbox_from_ellipse_(ell2);
    
    // double xmin = std::min(bb1(0, 0), bb2(0, 0));
    // double ymin = std::min(bb1(0, 1), bb2(0, 1));
    // double xmax = std::max(bb1(1, 0), bb2(1, 0));
    // double ymax = std::max(bb1(1, 1), bb2(1, 1));
    // double w2 = ell1.center.x() - bb1(0, 0);
    // double h2 = ell1.center.y() - bb1(0, 1);
    // xmin = ell1.center.x() - w2 * 1.5;
    // xmax = ell1.center.x() + w2 * 1.5;
    // ymin = ell1.center.y() - h2 * 1.5;
    // ymax = ell1.center.y() + h2 * 1.5;



    // xmin = 320-200;
    // ymin = 240-200;
    // xmax = 320+200;
    // ymax = 240+200;

    // xmin = ell1.center.x()-200;
    // ymin = ell1.center.y()-200;
    // xmax = ell1.center.x()+200;
    // ymax = ell1.center.y()+200;
    // xmin -= 100;
    // ymin -= 100;
    // xmax += 100;
    // ymax += 100;
    // std::cout << "global bbox: " << xmin << " " << ymin << " " << xmax << " " << ymax << "\n";


    // int N = 10;
    // double dx = (xmax-xmin) / (N-1);
    // double dy = (ymax-ymin) / (N-1);

    // std::cout << "dx = " << dx << "\n" << "dy = " << dy << "\n";
    // std::cout << "scale = " << scale << "\n";

    Ellipse_t ell1_norm = ell1;
    // ell1_norm.center.x() -= xmin;
    // ell1_norm.center.y() -= ymin;
    // ell1_norm.center.x() *= scale;
    // ell1_norm.center.y() *= scale;
    // ell1_norm.axes.x() *= scale;
    // ell1_norm.axes.y() *= scale;

    Ellipse_t ell2_norm = ell2;
    // ell2_norm.center.x() -= xmin;
    // ell2_norm.center.y() -= ymin;
    // ell2_norm.center.x() *= scale;
    // ell2_norm.center.y() *= scale;
    // ell2_norm.axes.x() *= scale;
    // ell2_norm.axes.y() *= scale;

    // dx *= scale;
    // dy *= scale;



    Eigen::Matrix2d R1;
    R1 << std::cos(ell1_norm.angle), -std::sin(ell1_norm.angle),
          std::sin(ell1_norm.angle), std::cos(ell1_norm.angle);
    

    Eigen::Matrix2d R2;
    R2 << std::cos(ell2_norm.angle), -std::sin(ell2_norm.angle),
          std::sin(ell2_norm.angle), std::cos(ell2_norm.angle);
    

    Eigen::Matrix2d A1;
    A1 << 1.0 / std::pow(ell1_norm.axes(0), 2), 0.0, 
          0.0, 1.0 / std::pow(ell1_norm.axes(1), 2);
    Eigen::Matrix2d A2;
    A2 << 1.0 / std::pow(ell2_norm.axes(0), 2), 0.0, 
          0.0, 1.0 / std::pow(ell2_norm.axes(1), 2);


// std::cout << "A1 = \n" << A1 << "\n";
// std::cout << "A2 = \n" << A2 << "\n";
    /////////////////////////////////////////////////////// scale the ellipses center somewhere !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Eigen::Matrix2d D1 = R1 * A1 * R1.transpose();
    Eigen::Matrix2d D2 = R2 * A2 * R2.transpose();

    // std::cout << "D1 = \n" << D1 << "\n";
    // std::cout << "D2 = \n" << D2 << "\n";


    
    // std::ofstream fout("debug.txt");
    

    std::vector<std::vector<double>> points = generate_sampling_points(ell1, N_az, N_dist, sampling_scale);
    // std::vector<std::vector<double>> points2 = generate_sampling_points(ell2, N_az, N_dist);
    // points.insert(points.end(), points2.begin(), points2.end());


    std::vector<double> errors(points.size(), 0.0);
    double total_err = 0.0;
    int i = 0;
    for (const auto& pt : points)
    {
        // std::cout << i*dx << " " << j * dy << " ===> " << ell1_norm.center.transpose() << " " << ell2_norm.center.transpose() << "\n";
        // if (pt[0] < 0 || pt[0] >= 640 || pt[1] < 0 || pt[1] >= 480)
        //     continue;
        Eigen::Vector2d p(pt[0], pt[1]);

        Eigen::Vector2d pt1 = p - ell1_norm.center;
        Eigen::Vector2d pt2 = p - ell2_norm.center;

        double v1 = pt1.transpose() * D1 * pt1;
        double v2 = pt2.transpose() * D2 * pt2;
        // fout << v1 << " " << v2 << "\n";
        errors[i++] = v1 - v2;
        total_err += std::pow(v1-v2, 2);
    }
    errors.resize(i);
    // return std::make_tuple(std::sqrt(total_err/(N_az*N_dist)), errors, std::vector<double>({}), points);
    return std::make_tuple(total_err, errors, std::vector<double>({}), points);
}













std::tuple<double, std::vector<double>, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_sampling_metric_ell_uniform(const Ellipse_t& ell1, const Ellipse_t& ell2, const std::vector<std::vector<double>>& points)
{
    // ell1: detection
    // ell2: reprojection

    Bbox2 bb1 = bbox_from_ellipse_(ell1);
    double scale = 1.0 / std::max(ell1.axes[0], ell1.axes[1]);


    Ellipse_t ell1_norm = ell1;
    ell1_norm.center.x() *= scale;
    ell1_norm.center.y() *= scale;
    ell1_norm.axes.x() *= scale;
    ell1_norm.axes.y() *= scale;

    Ellipse_t ell2_norm = ell2;
    ell2_norm.center.x() *= scale;
    ell2_norm.center.y() *= scale;
    ell2_norm.axes.x() *= scale;
    ell2_norm.axes.y() *= scale;


    Eigen::Matrix2d R1;
    R1 << std::cos(ell1_norm.angle), -std::sin(ell1_norm.angle),
          std::sin(ell1_norm.angle), std::cos(ell1_norm.angle);


    Eigen::Matrix2d R2;
    R2 << std::cos(ell2_norm.angle), -std::sin(ell2_norm.angle),
          std::sin(ell2_norm.angle), std::cos(ell2_norm.angle);


    Eigen::Matrix2d A1;
    A1 << 1.0 / std::pow(ell1_norm.axes(0), 2), 0.0,
          0.0, 1.0 / std::pow(ell1_norm.axes(1), 2);
    Eigen::Matrix2d A2;
    A2 << 1.0 / std::pow(ell2_norm.axes(0), 2), 0.0,
          0.0, 1.0 / std::pow(ell2_norm.axes(1), 2);


    /////////////////////////////////////////////////////// scale the ellipses center somewhere !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Eigen::Matrix2d D1 = R1 * A1 * R1.transpose();
    Eigen::Matrix2d D2 = R2 * A2 * R2.transpose();

    // std::vector<std::vector<double>> points = generate_sampling_points(ell1, N_az, N_dist);
    // std::vector<std::vector<double>> points2 = generate_sampling_points(ell2, N_az, N_dist);
    // points.insert(points.end(), points2.begin(), points2.end());


    std::vector<double> errors(points.size(), 0.0);
    double total_err = 0.0;
    int i = 0;
    for (const auto& pt : points)
    {
        // std::cout << i*dx << " " << j * dy << " ===> " << ell1_norm.center.transpose() << " " << ell2_norm.center.transpose() << "\n";
        // if (pt[0] < 0 || pt[0] >= 640 || pt[1] < 0 || pt[1] >= 480)
        //     continue;
        Eigen::Vector2d pt_norm(pt[0] * scale, pt[1]*scale);

        Eigen::Vector2d pt1 = pt_norm - ell1_norm.center;
        Eigen::Vector2d pt2 = pt_norm - ell2_norm.center;

        double v1 = pt1.transpose() * D1 * pt1;
        double v2 = pt2.transpose() * D2 * pt2;
        // fout << v1 << " " << v2 << "\n";
        errors[i++] = v1 - v2;
        total_err += std::pow(v1-v2, 2);
    }
    errors.resize(i);
    // return std::make_tuple(std::sqrt(total_err/(N_az*N_dist)), errors, std::vector<double>({}), points);
    return std::make_tuple(total_err, errors, std::vector<double>({}), points);
}







std::tuple<double, std::vector<double>, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_sampling_metric_ell_iou(const Ellipse_t& ell1, const Ellipse_t& ell2, int N_az, int N_dist, double sampling_scale)
{
    // ell1: detection
    // ell2: reprojection

    Bbox2 bb1 = bbox_from_ellipse_(ell1);
    // double scale = 10.0 / std::max(bb1(1, 0) - bb1(0, 0), bb1(1, 1) - bb1(0, 1));
    double scale = 1.0 / std::max(ell1.axes[0], ell1.axes[1]);


    Ellipse_t ell1_norm = ell1;
    // ell1_norm.center.x() -= xmin;
    // ell1_norm.center.y() -= ymin;
    ell1_norm.center.x() *= scale;
    ell1_norm.center.y() *= scale;
    ell1_norm.axes.x() *= scale;
    ell1_norm.axes.y() *= scale;

    Ellipse_t ell2_norm = ell2;
    // ell2_norm.center.x() -= xmin;
    // ell2_norm.center.y() -= ymin;
    ell2_norm.center.x() *= scale;
    ell2_norm.center.y() *= scale;
    ell2_norm.axes.x() *= scale;
    ell2_norm.axes.y() *= scale;

    // dx *= scale;
    // dy *= scale;



    Eigen::Matrix2d R1;
    R1 << std::cos(ell1_norm.angle), -std::sin(ell1_norm.angle),
          std::sin(ell1_norm.angle), std::cos(ell1_norm.angle);


    Eigen::Matrix2d R2;
    R2 << std::cos(ell2_norm.angle), -std::sin(ell2_norm.angle),
          std::sin(ell2_norm.angle), std::cos(ell2_norm.angle);


    Eigen::Matrix2d A1;
    A1 << 1.0 / std::pow(ell1_norm.axes(0), 2), 0.0,
          0.0, 1.0 / std::pow(ell1_norm.axes(1), 2);
    Eigen::Matrix2d A2;
    A2 << 1.0 / std::pow(ell2_norm.axes(0), 2), 0.0,
          0.0, 1.0 / std::pow(ell2_norm.axes(1), 2);


    Eigen::Matrix2d D1 = R1 * A1 * R1.transpose();
    Eigen::Matrix2d D2 = R2 * A2 * R2.transpose();

    std::vector<double> errors(N_az*N_dist, 0.0);
    double total_err = 0.0;
    // std::ofstream fout("debug.txt");


    std::vector<std::vector<double>> points = generate_sampling_points(ell1, N_az, N_dist, sampling_scale);
    int i = 0;
    for (const auto& pt : points)
    {
        // std::cout << i*dx << " " << j * dy << " ===> " << ell1_norm.center.transpose() << " " << ell2_norm.center.transpose() << "\n";
        Eigen::Vector2d pt_norm(pt[0] * scale, pt[1]*scale);

        Eigen::Vector2d pt1 = pt_norm - ell1_norm.center;
        Eigen::Vector2d pt2 = pt_norm - ell2_norm.center;

        double v1 = pt1.transpose() * D1 * pt1;
        double v2 = pt2.transpose() * D2 * pt2;
        if (v1 <= 1.01)
        {
            v1 = 1.0;
        }
        else
        {
            std::cout << "======================> ERROR v1 = " << std::fixed << std::setprecision(8) << v1 << std::endl;
            v1 = 0.0;
        }

        if (v2 <= 1.0)
        {
            v2 = 1.0;
        }
        else
        {
            v2 = 0.0;
        }
        // std::cout << v1 << " " << v2 << "\n";
        // fout << v1 << " " << v2 << "\n";
        errors[i++] = v1 - v2;
        total_err += std::pow(v1-v2, 2);
    }

    return std::make_tuple(std::sqrt(total_err), errors, std::vector<double>({}), points);
    // return std::make_tuple(std::sqrt(total_err/(N_az*N_dist)), errors, std::vector<double>({}), points);
}



// Grid sampling version
std::tuple<double, std::vector<double>, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_sampling_metric(const Ellipse_t& ell1, const Ellipse_t& ell2, int N)
{
    // ell1: detection
    // ell2: reprojection



    Bbox2 bb1 = bbox_from_ellipse_(ell1);
    // double scale = 10.0 / std::max(bb1(1, 0) - bb1(0, 0), bb1(1, 1) - bb1(0, 1));
    double scale = 1.0 / std::max(ell1.axes[0], ell1.axes[1]);
    Bbox2 bb2 = bbox_from_ellipse_(ell2);
    
    double xmin = std::min(bb1(0, 0), bb2(0, 0));
    double ymin = std::min(bb1(0, 1), bb2(0, 1));
    double xmax = std::max(bb1(1, 0), bb2(1, 0));
    double ymax = std::max(bb1(1, 1), bb2(1, 1));
    double w2 = ell1.center.x() - bb1(0, 0);
    double h2 = ell1.center.y() - bb1(0, 1);
    xmin = ell1.center.x() - w2 * 1.0;
    xmax = ell1.center.x() + w2 * 1.0;
    ymin = ell1.center.y() - h2 * 1.0;
    ymax = ell1.center.y() + h2 * 1.0;



    // xmin = 320-200;
    // ymin = 240-200;
    // xmax = 320+200;
    // ymax = 240+200;

    // xmin = ell1.center.x()-200;
    // ymin = ell1.center.y()-200;
    // xmax = ell1.center.x()+200;
    // ymax = ell1.center.y()+200;
    // xmin -= 100;
    // ymin -= 100;
    // xmax += 100;
    // ymax += 100;
    // std::cout << "global bbox: " << xmin << " " << ymin << " " << xmax << " " << ymax << "\n";


    // int N = 10;
    double dx = (xmax-xmin) / (N-1);
    double dy = (ymax-ymin) / (N-1);

    // std::cout << "dx = " << dx << "\n" << "dy = " << dy << "\n";
    // std::cout << "scale = " << scale << "\n";

    Ellipse_t ell1_norm = ell1;
    ell1_norm.center.x() -= xmin;
    ell1_norm.center.y() -= ymin;
    ell1_norm.center.x() *= scale;
    ell1_norm.center.y() *= scale;
    ell1_norm.axes.x() *= scale;
    ell1_norm.axes.y() *= scale;

    Ellipse_t ell2_norm = ell2;
    ell2_norm.center.x() -= xmin;
    ell2_norm.center.y() -= ymin;
    ell2_norm.center.x() *= scale;
    ell2_norm.center.y() *= scale;
    ell2_norm.axes.x() *= scale;
    ell2_norm.axes.y() *= scale;

    dx *= scale;
    dy *= scale;



    Eigen::Matrix2d R1;
    R1 << std::cos(ell1_norm.angle), -std::sin(ell1_norm.angle),
          std::sin(ell1_norm.angle), std::cos(ell1_norm.angle);
    

    Eigen::Matrix2d R2;
    R2 << std::cos(ell2_norm.angle), -std::sin(ell2_norm.angle),
          std::sin(ell2_norm.angle), std::cos(ell2_norm.angle);
    

    Eigen::Matrix2d A1;
    A1 << 1.0 / std::pow(ell1_norm.axes(0), 2), 0.0, 
          0.0, 1.0 / std::pow(ell1_norm.axes(1), 2);
    Eigen::Matrix2d A2;
    A2 << 1.0 / std::pow(ell2_norm.axes(0), 2), 0.0, 
          0.0, 1.0 / std::pow(ell2_norm.axes(1), 2);


// std::cout << "A1 = \n" << A1 << "\n";
// std::cout << "A2 = \n" << A2 << "\n";
    /////////////////////////////////////////////////////// scale the ellipses center somewhere !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Eigen::Matrix2d D1 = R1 * A1 * R1.transpose();
    Eigen::Matrix2d D2 = R2 * A2 * R2.transpose();

    // std::cout << "D1 = \n" << D1 << "\n";
    // std::cout << "D2 = \n" << D2 << "\n";


    
    std::vector<double> errors(N*N, 0.0);
    double total_err = 0.0;
    // std::ofstream fout("debug.txt");
    
    std::vector<std::vector<double>> points(N*N);
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            // std::cout << i*dx << " " << j * dy << " ===> " << ell1_norm.center.transpose() << " " << ell2_norm.center.transpose() << "\n";
            Eigen::Vector2d pt1(j * dx - ell1_norm.center.x(), i * dy - ell1_norm.center.y());
            Eigen::Vector2d pt2(j * dx - ell2_norm.center.x(), i * dy - ell2_norm.center.y());
            // fout << pt1.x() << " " << pt1.y() << " " << pt2.x() << " " << pt2.x() << "\n";

            points[i*N+j] = {j*dx/scale + xmin, i*dy/scale + ymin};
            // std::cout << pt1.transpose() << " " << pt2.transpose() << "\n";
            // pt1 *= scale;
            // pt2 *= scale;
            double v1 = pt1.transpose() * D1 * pt1;
            double v2 = pt2.transpose() * D2 * pt2;
            // fout << v1 << " " << v2 << "\n";
            errors[i*N+j] = v1 - v2;
            total_err += std::pow(v1-v2, 2);
        }
    }
//     std::cout << total_err << "\n";
    // fout.close();
    return std::make_tuple(std::sqrt(total_err/(N*N)), errors, std::vector<double>({xmin, ymin, xmax, ymax}), points);

}


Eigen::Vector2d find_closest_point_ellipse_contour(const Ellipse_t& ell, const Eigen::Vector2d p)
{

    Eigen::Vector2d p_cent = p - ell.center;
    Eigen::Matrix2d R;
    R << std::cos(ell.angle), -std::sin(ell.angle),
         std::sin(ell.angle), std::cos(ell.angle);

    Eigen::Vector2d p_loc = R.transpose() * p_cent;


    double a = ell.axes[0];
    double b = ell.axes[1];

    double px = std::fabs(p_loc.x());
    double py = std::fabs(p_loc.y());

    double t = M_PI_4;

    double x, y;
    for (int i = 0; i < 3; ++i)
    {
        x = a * std::cos(t);
        y = b * std::sin(t);

        double ex = (a*a - b*b) * std::pow(std::cos(t), 3) / a;
        double ey = (b*b - a*a) * std::pow(std::sin(t), 3) / b;

        double rx = x - ex;
        double ry = y - ey;

        double qx = px - ex;
        double qy = py - ey;

        double r = std::hypot(ry, rx);
        double q = std::hypot(qy, qx);

        double delta_c = r * std::asin((rx*qy- ry*qx) / (r*q));
        double delta_t = delta_c / std::sqrt(a*a + b*b - x*x - y*y);

        t += delta_t;
        t = std::min(M_PI_2, std::max(0.0, t));
    }
    Eigen::Vector2d p_res(std::copysign(x, p_loc.x()), std::copysign(y, p_loc.y()));


    Eigen::Vector2d p_res_rot = R * p_res;
    return p_res_rot + ell.center;
}




/***
 * The distance is computed form the points sampled on ellipse1 to
 * their corresponding closest points on ellipse 2.
 * */
std::tuple<double, std::vector<double>, std::vector<std::vector<double>>>
 distance_to_ellipse_contour_(const Ellipse_t& ell1, const Ellipse_t& ell2, int sampling)
{
    Point2d_list points = sample_ellipse_points(ell1, sampling);
    double error = 0.0;
    std::vector<double> errors(points.size()*2, 0.0);
    std::vector<std::vector<double>> matches(points.size(), std::vector<double>(4, 0.0));
    for (int i = 0; i < points.size(); ++i)
    {
        Eigen::Vector2d p = find_closest_point_ellipse_contour(ell2, points[i]);
        double dx = points[i].x() - p.x();
        double dy = points[i].y() - p.y();
        error += dx*dx + dy*dy;
        errors[i*2] = dx;
        errors[i*2+1] = dy;
        matches[i][0] = points[i].x();
        matches[i][1] = points[i].y();
        matches[i][2] = p.x();
        matches[i][3] = p.y();
    }
    return std::make_tuple(error, errors, matches);
}


Point2d_list order_points(const Ellipse_t& ell)
{
    Point2d_list extremities(4);
    Eigen::Vector2d p0(std::cos(ell.angle) * ell.axes[0], std::sin(ell.angle) * ell.axes[0]);
    Eigen::Vector2d p1(std::cos(ell.angle + M_PI_2) * ell.axes[1], std::sin(ell.angle + M_PI_2) * ell.axes[1]);
    Eigen::Vector2d p2(std::cos(ell.angle + M_PI) * ell.axes[0], std::sin(ell.angle + M_PI) * ell.axes[0]);
    Eigen::Vector2d p3(std::cos(ell.angle - M_PI_2) * ell.axes[1], std::sin(ell.angle - M_PI_2) * ell.axes[1]);

    extremities[0] = p0;
    extremities[1] = p1;
    extremities[2] = p2;
    extremities[3] = p3;

    int i = 0;
    while (i < 4)
    {
        if (extremities[i].x() >= 0 && extremities[i].y() <= 0)
            break;
        ++i;
    }
    Point2d_list ordered(4);
    for (int j = 0; j < 4; ++j)
    {
        ordered[j] = extremities[(i+j) % 4] + ell.center;
    }
    return ordered;
}


std::tuple<double, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_tangency_error_(const Ellipse_t& ell1, const Ellipse_t& ell2)
{
    auto pts1 = order_points(ell1);
    auto pts2 = order_points(ell2);

    double error = 0.0;
    std::vector<double> errors(8, 0.0);
    std::vector<std::vector<double>> matches(4, std::vector<double>(4, 0.0));
    for (int i = 0; i < 4; ++i)
    {
        double dx = pts1[i].x() - pts2[i].x();
        double dy = pts1[i].y() - pts2[i].y();
        error += dx*dx + dy*dy;
        errors[i*2] = dx;
        errors[i*2+1] = dy;
        matches[i][0] = pts1[i].x();
        matches[i][1] = pts1[i].y();
        matches[i][2] = pts2[i].x();
        matches[i][3] = pts2[i].y();
    }
    return std::make_tuple(error, errors, matches);
}




double gaussian_wasserstein_2d(const Ellipse_t& ell1, const Ellipse_t& ell2)
{
    auto g1 = ellipse_as_gaussian(ell1);
    const Eigen::Vector2d& mu1 = g1.first;
    const Eigen::Matrix2d& sigma1 = g1.second;
    auto g2 = ellipse_as_gaussian(ell2);
    const Eigen::Vector2d& mu2 = g2.first;
    const Eigen::Matrix2d& sigma2 = g2.second;


    Eigen::Matrix2d sigma11 = sigma1.sqrt();
    Eigen::Matrix2d s121 = sigma11 * sigma2 * sigma11;
    Eigen::Matrix2d sigma121 = s121.sqrt();

    double d = (mu1-mu2).squaredNorm() + (sigma1 + sigma2 - 2 * sigma121).trace();
    return d;
}



double bhattacharyya_distance(const Ellipse_t& ell1, const Ellipse_t& ell2)
{
    auto g1 = ellipse_as_gaussian(ell1);
    const Eigen::Vector2d& mu1 = g1.first;
    const Eigen::Matrix2d& sigma1 = g1.second;
    auto g2 = ellipse_as_gaussian(ell2);
    const Eigen::Vector2d& mu2 = g2.first;
    const Eigen::Matrix2d& sigma2 = g2.second;

    Eigen::Matrix2d G = (sigma1 + sigma2) * 0.5;
    Eigen::Matrix2d G_inv = G.inverse();
    Eigen::Vector2d mu_diff = mu1 - mu2;
    Eigen::Vector2d temp = G_inv * mu_diff;
    double dist = 0.125 * mu_diff.dot(temp);
    dist += 0.5 * std::log(G.determinant() / std::sqrt(sigma1.determinant() * sigma2.determinant()));
    return dist;
}

double bbox_area(const Bbox2& bb)
{
    return (bb(1, 0) - bb(0, 0)) * (bb(1, 1) - bb(0, 1));
}

double generalized_iou_(const Ellipse_t& ell1, const Ellipse_t& ell2)
{
    double iou = compute_iou_toms(ell1, ell2);
    Bbox2 bb1 = bbox_from_ellipse_(ell1);
    Bbox2 bb2 = bbox_from_ellipse_(ell2);

    double area1 = bbox_area(bb1);
    double area2 = bbox_area(bb2);

    Bbox2 bb_global;
    bb_global(0, 0) = std::min(bb1(0, 0), bb2(0, 0));
    bb_global(0, 1) = std::min(bb1(0, 1), bb2(0, 1));
    bb_global(1, 0) = std::max(bb1(1, 0), bb2(1, 0));
    bb_global(1, 1) = std::max(bb1(1, 1), bb2(1, 1));

    Bbox2 bb_inter;
    bb_inter(0, 0) = std::max(bb1(0, 0), bb2(0, 0));
    bb_inter(0, 1) = std::max(bb1(0, 1), bb2(0, 1));
    bb_inter(1, 0) = std::min(bb1(1, 0), bb2(1, 0));
    bb_inter(1, 1) = std::min(bb1(1, 1), bb2(1, 1));

    double area_global = bbox_area(bb_global);
    double area_inter = std::max(0.0, bbox_area(bb_inter));

    double area_union = area1 + area2 - area_inter;
    return iou - (area_global - area_union) / area_global;
}