/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ellipsoid.h"

#include <iostream>

#include <cmath>

#include "ellipse.h"

Ellipsoid_t decompose_ellipsoid(const Ellipsoid_m& Q)
{
    // assume Q(3, 3) = -1.0
    Ellipsoid_t ell;
    ell.center = -Q.block<3, 1>(0, 3);
    Eigen::Matrix4d T_c = Eigen::Matrix4d::Identity();
    T_c(0, 3) = -ell.center[0];
    T_c(1, 3) = -ell.center[1];
    T_c(2, 3) = -ell.center[2];
    Ellipsoid_m Q_center = T_c * Q * T_c.transpose();
    Ellipsoid_m Q_center_valid = Q_center;
    Q_center_valid += Q_center.transpose();
    Q_center_valid *= 0.5;
    
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigen_solver(Q_center_valid.block<3, 3>(0, 0));
    Eigen::Matrix3d eig_vectors = eigen_solver.eigenvectors();
    Eigen::Vector3d eig_values = eigen_solver.eigenvalues();
    
    if (eig_vectors.determinant() < 0.0) {
        eig_vectors.col(2) *= -1;
    }

    ell.axes = eig_values.cwiseAbs().cwiseSqrt();
    ell.orientation = eig_vectors;

    return ell;
}



Ellipsoid_t_list decompose_ellipsoids(const Ellipsoid_m_list& ellipsoids)
{
    Ellipsoid_t_list decomposed(ellipsoids.size());
    for (int i = 0; i < ellipsoids.size(); ++i)
    {
        decomposed[i] = decompose_ellipsoid(ellipsoids[i]);
    }
    return decomposed;    
}

Ellipsoid_e_list compute_ellipsoids_euclidean_forms(const Ellipsoid_t_list& ellipsoids)
{
    Ellipsoid_e_list euclideans(ellipsoids.size());
    for (int i = 0; i < ellipsoids.size(); ++i)
    {
        const Eigen::Vector3d c = ellipsoids[i].axes;
        Eigen::DiagonalMatrix<double, 3> A(1.0/std::pow(c[0], 2), 1.0/std::pow(c[1], 2), 1.0/std::pow(c[2], 2));
        euclideans[i] = ellipsoids[i].orientation * A * ellipsoids[i].orientation.transpose();
    }
    return euclideans;
}

Ellipse_t_list project_ellipsoids(const Ellipsoid_m_list& ellipsoids, const Projection_m& P)
{
    Ellipse_t_list ellipses(ellipsoids.size());
    for (int i = 0; i < ellipsoids.size(); ++i)
    {
        Eigen::Matrix3d C = P * ellipsoids[i] * P.transpose();
        ellipses[i] = decompose_ellipse(0.5 * (C + C.transpose()));
    }
    return ellipses;
}