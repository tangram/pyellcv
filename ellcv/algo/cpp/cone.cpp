/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cone.h"

#include <iostream>
#include <cmath>


Cone_axis cone_principal_axis(const Cone_m& cone)
{
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigen_solver(cone);
    Eigen::Vector3d eigvals = eigen_solver.eigenvalues();
    Eigen::Matrix3d eig_vectors = eigen_solver.eigenvectors();
    if (eigvals[0]*eigvals[1] > 0 && eigvals[1]*eigvals[2] > 0)
    {
        std::cerr << "Error all eigen values have the same sign" << std::endl;
        return Cone_axis(0.0, 0.0, 0.0);
    }
    int index; // find the eigen value with a different sign
    if (eigvals[1]*eigvals[2] > 0) {
        index = 0;
    } else if (eigvals[0]*eigvals[2] > 0) {
        index = 1;
    } else {
        index = 2;
    }
    Eigen::Vector3d axis = eig_vectors.col(index);
    axis.normalize();
    if (axis[2] < 0)
        axis *= -1.0;
    return axis;
}




Cone_m backprojection_cone_from_ellipse(const Ellipse_t& ell, const Eigen::Matrix3d& K_inv)
{
    double ca = std::cos(ell.angle);
    double sa = std::sin(ell.angle);
    double a = std::sqrt(std::pow(K_inv(0, 0) * ell.axes[0] * ca, 2) + 
                         std::pow(K_inv(1, 1) * ell.axes[0] * sa, 2));
    double b = std::sqrt(std::pow(K_inv(0, 0) * ell.axes[1] * -sa, 2) + 
                         std::pow(K_inv(1, 1) * ell.axes[1] * ca, 2));
    Eigen::Vector3d Ec = Eigen::Vector3d::Zero();
    Eigen::Vector3d Kc = K_inv * ell.center.homogeneous();
    Eigen::Vector3d Uc(ca, sa, 1.0);
    Eigen::Vector3d Vc(-sa, ca, 1.0);
    Eigen::Vector3d Nc(0.0, 0.0, 1.0);
    Eigen::Matrix3d M = (Uc * Uc.transpose()) / std::pow(a, 2);
    M += (Vc * Vc.transpose()) / std::pow(b, 2);
    Eigen::Vector3d W(0.0, 0.0, 1.0/(Kc[2]-Ec[2]));
    Eigen::Matrix3d P = (Ec-Kc) * W.transpose() + Eigen::Matrix3d::Identity();
    Eigen::Matrix3d Q = W * W.transpose();
    return P.transpose() * M * P - Q;
}

Cone_m_list_of_list compute_backprojection_cones_from_ellipses(const Ellipse_t_list_of_list& ellipses, const Eigen::Matrix3d& K_inv)
{
    Cone_m_list_of_list cones(ellipses.size());

    for (int i = 0; i < ellipses.size(); ++i)
    {
        for (const auto& ell : ellipses[i])
        {
            cones[i].push_back(backprojection_cone_from_ellipse(ell, K_inv));
        }
    }
    return cones;
}

Cone_axis_list_of_list compute_cones_axis(const Cone_m_list_of_list& cones)
{
    Cone_axis_list_of_list axes(cones.size());

    for (int i = 0; i < cones.size(); ++i)
    {
        for (const auto& c : cones[i])
        {
            axes[i].push_back(cone_principal_axis(c));
        }
    }
    return axes;
}