/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cmath>
#include <complex>
#include <iomanip>
#include <mutex>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>

#include <Eigen/Dense>

#include "types.h"
#include "cone.h"
#include "ellipse.h"
#include "ellipsoid.h"
#include "iou/ee.h"
#include "metrics.h"

#include "p3p.h"
#include "threadPool.h"

namespace py = pybind11;

#define TO_RAD(X) 0.01745329251*X
#define TO_DEG(X) 57.2957795131*X



double tangency_error(const Ellipse_t& ell0, const Ellipse_t ell1)
{
    Eigen::Vector2d axes0 = ell0.axes, axes1 = ell1.axes;
    double angle0 = ell0.angle, angle1 = ell1.angle;
    if (axes0[0] > axes0[1] && axes1[0] < axes1[1] ||
        axes0[0] < axes0[1] && axes1[0] > axes1[1])
        {
            std::swap(axes1[0], axes1[1]);
            angle1 += M_PI/2;
        }

    Eigen::Vector2d p00(std::cos(angle0) * axes0[0], std::sin(angle0) * axes0[1]);
    Eigen::Vector2d p10(std::cos(angle1) * axes1[0], std::sin(angle1) * axes1[1]);
    Eigen::Vector2d p11(std::cos(angle1+M_PI) * axes1[0], std::sin(angle1+M_PI) * axes1[1]);
    if ((p10 - p00).squaredNorm() > (p11 - p00).squaredNorm())
    {
        angle1 += M_PI;
    }

    Eigen::Matrix2d R0, R1;
    double c0 = std::cos(angle0);
    double s0 = std::sin(angle0);
    double c1 = std::cos(angle1);
    double s1 = std::sin(angle1);
    R0 << c0, -s0,
          s0, c0;
    R1 << c1, -s1,
          s1, c1;

    double max_dist = 0;
    double sum_dist = 0;
    double alpha_step = TO_RAD(20);
    int count = 0;
    for (double alpha = 0.0; alpha < M_PI*2; alpha += alpha_step)
    {
        double ca = std::cos(alpha);
        double sa = std::sin(alpha);
        Eigen::Vector2d p0 = R0 * Eigen::Vector2d(axes0.x() * ca, axes0.y() * sa);
        Eigen::Vector2d p1 = R1 * Eigen::Vector2d(axes1.x() * ca, axes1.y() * sa);
        p0 += ell0.center;
        p1 += ell1.center;
        double d = (p0 - p1).norm();
        max_dist = std::max(max_dist, d);
        sum_dist += d;
        ++count;
    }
    // return sum_dist / count;
    return max_dist;
}

std::pair<Mapping, Mapping> generate_possible_mappings(const std::vector<int> objects_category, const std::vector<int> detections_category)
{
    // generate objects category <-> id mapping
    std::unordered_map<int, std::vector<int>> obj_cat_id_mapping;
    for (int i = 0; i < objects_category.size(); ++i)
    {
        auto cat = objects_category[i];
        if (obj_cat_id_mapping.find(cat) != obj_cat_id_mapping.end())
        {
            obj_cat_id_mapping[cat].push_back(i);
        }
        else
        {
            obj_cat_id_mapping[cat] = {i};
        }
    }

    // generate detections category <-> id mapping
    std::unordered_map<int, std::vector<int>> det_cat_id_mapping;
    for (int i = 0; i < detections_category.size(); ++i)
    {
        auto cat = detections_category[i];
        if (det_cat_id_mapping.find(cat) != det_cat_id_mapping.end())
        {
            det_cat_id_mapping[cat].push_back(i);
        }
        else
        {
            det_cat_id_mapping[cat] = {i};
        }
    }


    // Generate all possible mapping (detection => possible objects)
    Mapping det_to_obj_mapping(detections_category.size());
    for (int i = 0; i < detections_category.size(); ++i)
    {
        det_to_obj_mapping[i] = obj_cat_id_mapping[detections_category[i]];
    }



    // Generate all possible mapping (objects => possible detections)
    Mapping obj_to_det_mapping(objects_category.size());
    for (int i = 0; i < objects_category.size(); ++i)
    {
        obj_to_det_mapping[i] = det_cat_id_mapping[objects_category[i]];
    }


    return std::make_pair(det_to_obj_mapping, obj_to_det_mapping);
}

std::pair<bool, Eigen::Matrix3d> gram_schmidt_orthonormalisation(const Eigen::Vector3d& u, const Eigen::Vector3d& v)
{
    Eigen::Matrix3d basis = Eigen::Matrix3d::Identity();
    basis.col(0) = u.normalized();
    Eigen::Vector3d w = u.cross(v);
    if (w.squaredNorm() < 1e-8) {
        std::cerr << "Error: gram schmidt orthonormalization: v1 and v2 are collinear" << std::endl;
        return std::make_pair(false, basis);
    }
    basis.col(2) = w.normalized();
    Eigen::Vector3d v2 = w.cross(u);
    basis.col(1) = v2.normalized();
    return std::make_pair(true, basis);
}

double camera_roll(const Eigen::Matrix3d& R)
{
    double r0 = std::atan2(R(2, 1), R(2, 0));
    double r1 = r0 + M_PI;
    if (r1 > M_PI)
        r1 -= 2 * M_PI;
    if (R(2, 0) * std::cos(r0) + R(2, 1) * std::sin(r0) > R(2, 0) * std::cos(r1) + R(2, 1) * std::sin(r1))
        r0 = r1;
    r0 -= M_PI / 2;
    if (r0 < -M_PI)
        r0 += 2 * M_PI;
    if (r0 > M_PI)
        r0 -= 2 * M_PI;
    return -r0;
}

std::vector<Eigen::Matrix3d> generate_possible_orientations(const Eigen::Vector3d X0, const Eigen::Vector3d X1, const Cone_axis& axis0, const Cone_axis& axis1)
{
    std::vector<Eigen::Matrix3d> orientations;
    Eigen::Vector3d C_w = X1 - X0;
    C_w.normalize();


    std::pair<bool, Eigen::Matrix3d> ret = gram_schmidt_orthonormalisation(axis0, axis1);
    Eigen::Matrix3d& new_basis = ret.second;

    if (!ret.first)
    {
        return {};
    };
    // make sure u3 goes up (its "y" coordinate is negative in the camera coordinates)
    if (new_basis(1, 2) > 0) {
        new_basis.col(1) *= -1.0;
        new_basis.col(2) *= -1.0;
    }

    Eigen::Vector2d u = new_basis.block<1, 2>(0, 0);
    double norm_u = u.norm();
    u /= norm_u;
    double delta = std::atan2(u[1], u[0]);

    // Here, we assume theta1 = 0.0 and theta2 = 0.0. See the Python implementation for
    // the details without this assumption

    double alpha = 0.0;
    double angle_step = TO_RAD(1.0);
    while (alpha < 2*M_PI)
    {
        Eigen::Vector3d v1(std::cos(alpha), std::sin(alpha), 0.0);
        
        Eigen::Matrix3d Pw_int;
        Pw_int << v1, C_w, v1.cross(C_w);

        double cos_gamma = Pw_int.col(0).dot(Pw_int.col(1));
        double cos_epsilon = cos_gamma / norm_u;

        if (std::fabs(cos_epsilon) > 1)
        {
            alpha += angle_step;
            continue;
        }

        for (int j : {1, -1})
        {
            double epsilon = j * std::acos(cos_epsilon);
            double beta = delta - epsilon;

            Eigen::Vector3d M(std::cos(beta), std::sin(beta), 0.0);
            Eigen::Vector3d v1 = Eigen::Vector3d::UnitX();
            Eigen::Vector3d v2 = new_basis * M;
            Eigen::Matrix3d Pc_int;
            Pc_int << v1, v2, v1.cross(v2);
            Eigen::Matrix3d Pint_c = Pc_int.inverse();

            Eigen::Matrix3d Rw_c = Pw_int * Pint_c;

            // Check camera roll and ignore cases where roll != 0
            if (std::fabs(camera_roll(Rw_c)) > 1e-3)
                continue;

            orientations.push_back(Rw_c);
        }
        alpha += angle_step;
    }

    if (std::fabs(C_w[2]) < 0.02)
    {
        C_w[2] = 0.0;
        C_w.normalize();

        double alpha = 0.0;
        while (alpha < 2*M_PI)
        {
            for (int j : {1, -1})
            {
                Eigen::Vector3d v1 = j * C_w;
                double sa = std::sin(alpha);
                Eigen::Vector3d v2(-sa * C_w[1], sa * C_w[0], std::cos(alpha));
                Eigen::Matrix3d Rw_c;
                Rw_c << v1, v2, v1.cross(v2);

                // Check camera roll and ignore cases where roll != 0
                if (std::fabs(camera_roll(Rw_c)) > 1e-3)
                    continue;
    
                orientations.push_back(Rw_c);
            }
            alpha += angle_step;
        }
    }

    return orientations;
}



Eigen::Vector3d compute_position_from_2d3d(const Eigen::Vector2d& xn0, const Eigen::Vector2d& xn1,
                                           const Eigen::Vector3d& X0, const Eigen::Vector3d& X1,
                                           const Eigen::Matrix3d& Rw_c)
{
    Eigen::Matrix<double, 4, 3> A;
    Eigen::Vector4d b;
    A << 0.0, -1.0, xn0.y(),
         1.0, 0.0, -xn0.x(),
         0.0, -1.0, xn1.y(),
         1.0, 0.0, -xn1.x();
    Eigen::Vector3d RX0 = Rw_c.transpose() * X0;
    Eigen::Vector3d RX1 = Rw_c.transpose() * X1;
    b << RX0.y() - xn0.y() * RX0.z(),
         xn0.x() * RX0.z() - RX0.x(),
         RX1.y() - xn1.y() * RX1.z(),
         xn1.x() * RX1.z() - RX1.x();
    
    // The pseudo-inverse is the fastest method but the least accurate (if A is ill-conditionned)
    // Eigen::Vector3d t_est = (A.transpose() * A).ldlt().solve(A.transpose() * b);
    // Using SVD decomposition instead is slower but more accurate
    Eigen::Vector3d t_est = A.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b);

    return -Rw_c * t_est;
}




double bbox_iou(const Bbox2& bb0, const Bbox2& bb1)
{
    double inter_x_min = std::max(bb0(0, 0), bb1(0, 0));
    double inter_y_min = std::max(bb0(0, 1), bb1(0, 1));
    double inter_x_max = std::min(bb0(1, 0), bb1(1, 0));
    double inter_y_max = std::min(bb0(1, 1), bb1(1, 1));
    if (inter_x_min >= inter_x_max || inter_y_min >= inter_y_max)
        return 0.0;
    double area0 = (bb0(1, 0) - bb0(0, 0)) * (bb0(1, 1) - bb0(0, 1));
    double area1 = (bb1(1, 0) - bb1(0, 0)) * (bb1(1, 1) - bb1(0, 1));
    double area_inter = (inter_x_max - inter_x_min) * (inter_y_max - inter_y_min);
    return area_inter / (area0 + area1 - area_inter);
}


Eigen::Matrix3d backprojection_cone_from_ellipse_test(const Ellipse_m& ell, const Eigen::Matrix3d& K_inv)
{
    return backprojection_cone_from_ellipse(decompose_ellipse(ell), K_inv);
}



std::tuple<int, Pose_m_list, std::vector<double>,
           std::vector<std::vector<std::pair<std::pair<int, int>, int>>>,
           std::vector<std::vector<std::pair<std::pair<int, int>, int>>>,
           std::vector<std::vector<double>>>
  solveP3P_ransac(const Ellipsoid_m_list& ellipsoids, const std::vector<int>& ellipsoids_category, const Bbox2_list& detections_bbox, const std::vector<int>& detections_category, const Ellipse_m_list_of_list& detections_ellipses,
       const Eigen::Matrix3d& K, double THRESHOLD)
{
    if (detections_bbox.size() < 3)
    {
        return std::make_tuple(-1, std::vector<Pose_m, Eigen::aligned_allocator<Pose_m>>(), std::vector<double>(),
        std::vector<std::vector<std::pair<std::pair<int, int>, int>>>(),
        std::vector<std::vector<std::pair<std::pair<int, int>, int>>>(),
        std::vector<std::vector<double>>());
    }
    Eigen::Matrix3d K_inv = K.inverse();

    std::vector<Eigen::Vector2d> detections_bbox_centers(detections_bbox.size());
    for (int i = 0; i < detections_bbox.size(); ++i)
    {
        detections_bbox_centers[i] = detections_bbox[i].colwise().mean();
    }

    Ellipsoid_t_list ellipsoids_decomposed = decompose_ellipsoids(ellipsoids);
    Ellipsoid_e_list ellipsoids_euclideans = compute_ellipsoids_euclidean_forms(ellipsoids_decomposed);

    Ellipse_t_list_of_list detections_ellipses_decomposed = decompose_ellipses(detections_ellipses);
    Cone_m_list_of_list cones = compute_backprojection_cones_from_ellipses(detections_ellipses_decomposed, K_inv);
    Cone_axis_list_of_list cones_axis = compute_cones_axis(cones);
    Bbox2_list_of_list detections_ellipses_bboxes = compute_ellipses_bboxes(detections_ellipses_decomposed);

    Point2d_list_of_list detections_ellipses_normalized_centers(detections_ellipses_decomposed.size());
    for (int i = 0; i < detections_ellipses_decomposed.size(); ++i)
    {
        for (const auto& ell : detections_ellipses_decomposed[i])
        {
            Eigen::Vector3d cn = K_inv * ell.center.homogeneous();
            detections_ellipses_normalized_centers[i].push_back(cn.head(2));
        }
    }

    std::pair<Mapping, Mapping> ret = generate_possible_mappings(ellipsoids_category, detections_category);
    Mapping &det_to_obj = ret.first, &obj_to_det = ret.second;


    Pose_m_list all_poses;
    std::vector<double> all_costs;
    int best_index = -1;
    double best_all_costs = std::numeric_limits<double>::infinity();
    std::vector<std::vector<std::pair<std::pair<int, int>, int>>> used_pairs;
    std::vector<std::vector<std::pair<std::pair<int, int>, int>>> used_inliers;

    // ThreadPool pool(1); //std::thread::hardware_concurrency());
    // std::vector< std::future<int> > threads;
    std::mutex lock;

    for (int det_0 = 0; det_0 < det_to_obj.size(); ++det_0)
    {
        for (int det_1 = det_0 + 1; det_1 < det_to_obj.size(); ++det_1)
        {
            for (int det_2 = det_1 + 1; det_2 < det_to_obj.size(); ++det_2)
            {
                for (auto obj_0 : det_to_obj[det_0])
                {
                    for (auto obj_1: det_to_obj[det_1])
                    {
                        if (obj_0 == obj_1) continue;

                        for (auto obj_2: det_to_obj[det_2])
                        {
                            if (obj_1 == obj_2 || obj_0 == obj_2) continue;

                            for (int pred_0 = 0; pred_0 < detections_ellipses[det_0].size(); ++pred_0)
                            {
                                for (int pred_1 = 0; pred_1 < detections_ellipses[det_1].size(); ++pred_1)
                                {
                                    for (int pred_2 = 0; pred_2 < detections_ellipses[det_2].size(); ++pred_2)
                                    {
                                        auto process_fct = [obj_0, obj_1, obj_2,
                                                            det_0, det_1, det_2,
                                                            pred_0, pred_1, pred_2,
                                                            &cones_axis, &ellipsoids,
                                                            &ellipsoids_decomposed, &obj_to_det,
                                                            &detections_bbox, &detections_ellipses,
                                                            &detections_ellipses_decomposed,
                                                            &all_costs, &all_poses,
                                                            &used_pairs, &used_inliers,
                                                            &best_all_costs, &best_index,
                                                            &K, &lock, &THRESHOLD]()
                                        {
                                            Eigen::Matrix3d X;
                                            X.col(0) = ellipsoids_decomposed[obj_0].center;
                                            X.col(1) = ellipsoids_decomposed[obj_1].center;
                                            X.col(2) = ellipsoids_decomposed[obj_2].center;
                                            Eigen::Matrix3d x;
                                            x.col(0) = cones_axis[det_0][pred_0];
                                            x.col(1) = cones_axis[det_1][pred_1];
                                            x.col(2) = cones_axis[det_2][pred_2];
                                            x.col(0).normalize();
                                            x.col(1).normalize();
                                            x.col(2).normalize();

                                            Eigen::Matrix<Eigen::Matrix<double, 3, 4>, 4, 1> solutions;
                                            monocular_pose_estimator::P3P::computePoses(x, X, solutions);

                                            double best_iou = 0.0, best_cost = std::numeric_limits<double>::infinity();
                                            std::vector<std::pair<std::pair<int, int>, int>> best_inliers;
                                            Pose_m best_pose;
                                            for (int idx = 0; idx < 4; ++idx)
                                            {
                                                const Eigen::Matrix<double, 3, 4>& p = solutions(idx, 0);
                                                Eigen::Matrix3d o = p.block<3, 3>(0, 0);
                                                Eigen::Vector3d pos_est = p.col(3);

                                                // Check that the camera is in front of the scene
                                                if ((ellipsoids_decomposed[obj_0].center - pos_est).dot(o.col(2)) < 0)
                                                    continue;

                                                // ----- Evaluate coherence -----
                                                // project all ellipsoids
                                                Pose_m pose, Rt;
                                                pose << o, pos_est;
                                                Rt << o.transpose(), -o.transpose() * pos_est;

                                                Projection_m P = K * Rt;
                                                Ellipse_t_list proj_ellipsoids = project_ellipsoids(ellipsoids, P);

                                                double cost_total = 0.0;
                                                std::vector<std::pair<std::pair<int, int>, int>> inliers;
                                                std::vector<bool> used(detections_bbox.size(), false);
                                                std::vector<double> costs(detections_bbox.size(), 1.0);
                                                for (int obj_id = 0; obj_id < ellipsoids.size(); ++obj_id)
                                                {
                                                    double max_iou = 0.0, max_inter, max_union, min_dist = std::numeric_limits<double>::infinity();
                                                    int best_det_id = 0, best_pred_id = 0;
                                                    // For the two ellipsoids used to compute the pose, only used prediction can be matched
                                                    if (obj_id == obj_0)
                                                    {
                                                        double iou = compute_iou_toms(detections_ellipses_decomposed[det_0][pred_0], proj_ellipsoids[obj_id]);
                                                        // double iou = tangency_error(detections_ellipses_decomposed[det_0][pred_0], proj_ellipsoids[obj_id]);
                                                        // auto bbox_proj = bbox_from_ellipse_(proj_ellipsoids[obj_id]);
                                                        // double iou = bbox_iou(detections_ellipses_bboxes[det_0][pred_0], bbox_proj);
                                                        // min_dist = iou;
                                                        max_iou = iou;
                                                        best_det_id = det_0;
                                                        best_pred_id = pred_0;
                                                        used[det_0] = true;
                                                    }
                                                    else if (obj_id == obj_1)
                                                    {
                                                        double iou = compute_iou_toms(detections_ellipses_decomposed[det_1][pred_1], proj_ellipsoids[obj_id]);
                                                        // double iou = tangency_error(detections_ellipses_decomposed[det_1][pred_1], proj_ellipsoids[obj_id]);
                                                        // auto bbox_proj = bbox_from_ellipse_(proj_ellipsoids[obj_id]);
                                                        // double iou = bbox_iou(detections_ellipses_bboxes[det_1][pred_1], bbox_proj);
                                                        // min_dist = iou;
                                                        max_iou = iou;
                                                        best_det_id = det_1;
                                                        best_pred_id = pred_1;
                                                        used[det_1] = true;
                                                    }
                                                    else if (obj_id == obj_2)
                                                    {
                                                        double iou = compute_iou_toms(detections_ellipses_decomposed[det_2][pred_2], proj_ellipsoids[obj_id]);
                                                        // double iou = tangency_error(detections_ellipses_decomposed[det_1][pred_1], proj_ellipsoids[obj_id]);
                                                        // auto bbox_proj = bbox_from_ellipse_(proj_ellipsoids[obj_id]);
                                                        // double iou = bbox_iou(detections_ellipses_bboxes[det_1][pred_1], bbox_proj);
                                                        // min_dist = iou;
                                                        max_iou = iou;
                                                        best_det_id = det_2;
                                                        best_pred_id = pred_2;
                                                        used[det_2] = true;
                                                    }
                                                    else
                                                    {
                                                        for (int det_id : obj_to_det[obj_id])
                                                        {
                                                            if (used[det_id])
                                                                continue;
                                                            for (int pred_id = 0; pred_id < detections_ellipses[det_id].size(); ++pred_id)
                                                            {
                                                                double iou = compute_iou_toms(detections_ellipses_decomposed[det_id][pred_id], proj_ellipsoids[obj_id]);
                                                                // double iou = tangency_error(detections_ellipses_decomposed[det_id][pred_id], proj_ellipsoids[obj_id]);
                                                                // auto bbox_proj = bbox_from_ellipse_(proj_ellipsoids[obj_id]);
                                                                // double iou = bbox_iou(detections_ellipses_bboxes[det_id][pred_id], bbox_proj);
                                                                if (iou > max_iou)
                                                                // if (iou < min_dist)
                                                                {
                                                                    max_iou = iou;
                                                                    // min_dist = iou;
                                                                    best_det_id = det_id;
                                                                    best_pred_id = pred_id;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    // if (min_dist < 50)
                                                    if (max_iou >= THRESHOLD || obj_id == obj_0 || obj_id == obj_1 || obj_id == obj_2)
                                                    {
                                                        inliers.push_back(std::make_pair(std::make_pair(best_det_id, best_pred_id), obj_id));
                                                        // avoid the same detection to be used several times
                                                        used[best_det_id] = true;
                                                        costs[best_det_id] = 1.0 - max_iou;
                                                    }
                                                }
                                                cost_total = std::accumulate(costs.begin(), costs.end(), 0.0);

                                                if (cost_total < best_cost)
                                                {
                                                    best_cost = cost_total;
                                                    best_inliers = inliers;
                                                    best_pose = pose;
                                                }
                                            }


                                            if (best_cost < detections_bbox.size())
                                            {
                                                // lock.lock();
                                                if (best_cost < best_all_costs)
                                                {
                                                    best_all_costs = best_cost;
                                                    best_index = all_costs.size();
                                                }
                                                all_costs.push_back(best_cost);
                                                all_poses.push_back(best_pose);
                                                used_pairs.push_back({std::make_pair(std::make_pair(det_0, pred_0), obj_0),
                                                                      std::make_pair(std::make_pair(det_1, pred_1), obj_1),
                                                                      std::make_pair(std::make_pair(det_2, pred_2), obj_2)});
                                                used_inliers.push_back(std::move(best_inliers));
                                                // lock.unlock();

                                            }
                                            return 0;
                                        };

                                        process_fct();
                                        // threads.emplace_back(pool.enqueue(process_fct));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // Wait the end of processing
    // for (auto&& th: threads)
    // {
    //     th.get();
    // }

    return std::make_tuple(best_index, std::move(all_poses), std::move(all_costs), std::move(used_pairs), std::move(used_inliers), std::vector<std::vector<double>>());
}




PYBIND11_MODULE(cpp, m) {
    m.doc() = R"pbdoc(
        Ellipses IoU
        -----------------------

        .. currentmodule:: Ellipses_IoU

        .. autosummary::
           :toctree: _generate
			compute_iou
    )pbdoc";

    m.def("solveP3P_ransac", &solveP3P_ransac, R"pbdoc(test)pbdoc");
    m.def("decompose_ellipse", &decompose_ellipse);
    m.def("decompose_ellipsoid", &decompose_ellipsoid);
    m.def("cone_principal_axis", &cone_principal_axis);
}
