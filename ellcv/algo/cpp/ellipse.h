/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ELLIPSE_H
#define ELLIPSE_H

#include <Eigen/Dense>

#include "types.h"

Ellipse_t decompose_ellipse(const Ellipse_m& C);

Ellipse_t_list_of_list decompose_ellipses(const Ellipse_m_list_of_list& ellipses);

Bbox2 bbox_from_ellipse_(const Ellipse_t& ell);

Bbox2_list_of_list compute_ellipses_bboxes(const Ellipse_t_list_of_list& ellipses);

Point2d_list sample_ellipse_points(const Ellipse_t& ell, int sampling);

std::pair<Eigen::Vector2d, Eigen::Matrix2d> ellipse_as_gaussian(const Ellipse_t& ell);

#endif // ELLIPSE_H
