/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ellipse.h"

#include <iostream>

#include <cmath>

Ellipse_t decompose_ellipse(const Ellipse_m& C)
{
    Ellipse_m CC = C / -C(2, 2);
    Ellipse_t ell;
    ell.center = -CC.block<2, 1>(0, 2);
    Eigen::Matrix3d T_c = Eigen::Matrix3d::Identity();
    T_c(0, 2) = -ell.center[0];
    T_c(1, 2) = -ell.center[1];
    Ellipse_m C_center = T_c * CC * T_c.transpose();
    Ellipse_m C_center_valid = C_center;
    C_center_valid += C_center.transpose();
    C_center_valid *= 0.5;
    
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix2d> eigen_solver(C_center_valid.block<2, 2>(0, 0));
    Eigen::Matrix2d eig_vectors = eigen_solver.eigenvectors();
    Eigen::Vector2d eig_values = eigen_solver.eigenvalues();
    
    if (eig_vectors.determinant() < 0.0) {
        eig_vectors.col(1) *= -1;
    }
    if (eig_vectors(0, 0) < 0) {    // force first axis to be positive in x
        eig_vectors *= -1.0;
    }

    ell.axes = eig_values.cwiseAbs().cwiseSqrt();
    ell.angle = std::atan2(eig_vectors(1, 0), eig_vectors(0, 0));

    return ell;
}

Ellipse_t_list_of_list decompose_ellipses(const Ellipse_m_list_of_list& ellipses)
{
    Ellipse_t_list_of_list decomposed(ellipses.size());
    for (int i = 0; i < ellipses.size(); ++i)
    {
        for (const auto& ell : ellipses[i])
        {
            decomposed[i].push_back(decompose_ellipse(ell));
        }
    }
    return decomposed;
}

Bbox2 bbox_from_ellipse_(const Ellipse_t& ell)
{
    double c = std::cos(ell.angle);
    double s = std::sin(ell.angle);
    double xmax = std::sqrt(std::pow(ell.axes[0]*c, 2) + std::pow(-ell.axes[1]*s, 2));
    double ymax = std::sqrt(std::pow(ell.axes[0]*s, 2) + std::pow( ell.axes[1]*c, 2));

    Bbox2 bb;
    bb << ell.center[0] - xmax, ell.center[1] - ymax,
          ell.center[0] + xmax, ell.center[1] + ymax;
    return bb;
}

Bbox2_list_of_list compute_ellipses_bboxes(const Ellipse_t_list_of_list& ellipses)
{
    Bbox2_list_of_list bboxes(ellipses.size());
    for (int i = 0; i < ellipses.size(); ++i)
    {
        for (const auto& ell : ellipses[i])
        {
            bboxes[i].push_back(bbox_from_ellipse_(ell));
        }
    }
    return bboxes;
}

Point2d_list sample_ellipse_points(const Ellipse_t& ell, int sampling)
{
    Eigen::Matrix2d R;
    R << std::cos(ell.angle), -std::sin(ell.angle),
         std::sin(ell.angle), std::cos(ell.angle);

    double delta_a = 2 * M_PI / sampling;

    Point2d_list points(sampling);
    double a = 0.0;
    for (int i = 0; i < sampling; ++i)
    {
        Eigen::Vector2d p(ell.axes[0] * std::cos(i*delta_a), ell.axes[1] * std::sin(i*delta_a));
        Eigen::Vector2d p_rot = R * p;
        points[i][0] = p_rot.x() + ell.center.x();
        points[i][1] = p_rot.y() + ell.center.y();
    }
    return points;
}

std::pair<Eigen::Vector2d, Eigen::Matrix2d> ellipse_as_gaussian(const Ellipse_t& ell)
{
    Eigen::Matrix2d A_dual;
    A_dual << std::pow(ell.axes[0], 2), 0.0,
              0.0, std::pow(ell.axes[1], 2);

    Eigen::Matrix2d R;
    R << std::cos(ell.angle), -std::sin(ell.angle),
         std::sin(ell.angle), std::cos(ell.angle);
    Eigen::Matrix2d cov = R * A_dual * R.transpose();
    return std::make_pair(ell.center, cov);
}
