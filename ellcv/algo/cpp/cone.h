/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONE_H
#define CONE_H

#include <Eigen/Dense>

#include "types.h"

Cone_axis cone_principal_axis(const Eigen::Matrix<double, 3, 3>& cone);

Cone_m backprojection_cone_from_ellipse(const Ellipse_t& ell, const Eigen::Matrix3d& K_inv);

Cone_m_list_of_list compute_backprojection_cones_from_ellipses(const Ellipse_t_list_of_list& ellipses, const Eigen::Matrix3d& K_inv);
Cone_axis_list_of_list compute_cones_axis(const Cone_m_list_of_list& cones);

#endif // CONE_H
