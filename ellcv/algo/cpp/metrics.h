#ifndef METRICS_H
#define METRICS_H

#include <Eigen/Dense>

#include "types.h"

std::vector<std::vector<double>>
 generate_sampling_points(const Ellipse_t& ell, int count_az, int count_dist, double scale);

std::tuple<double, std::vector<double>, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_sampling_metric_ell(const Ellipse_t& ell1, const Ellipse_t& ell2, int N_az, int N_dist, double sampling_scale);

std::tuple<double, std::vector<double>, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_sampling_metric_ell_uniform(const Ellipse_t& ell1, const Ellipse_t& ell2, const std::vector<std::vector<double>>& points);

 std::tuple<double, std::vector<double>, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_sampling_metric_ell_iou(const Ellipse_t& ell1, const Ellipse_t& ell2, int N_az, int N_dist, double sampling_scale);

std::tuple<double, std::vector<double>, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_sampling_metric(const Ellipse_t& Q1, const Ellipse_t& Q2, int N);

Eigen::Vector2d find_closest_point_ellipse_contour(const Ellipse_t& ell, const Eigen::Vector2d p);
std::tuple<double, std::vector<double>, std::vector<std::vector<double>>>
 distance_to_ellipse_contour_(const Ellipse_t& ell1, const Ellipse_t& ell2, int sampling);
 std::tuple<double, std::vector<double>, std::vector<std::vector<double>>>
 ellipses_tangency_error_(const Ellipse_t& ell1, const Ellipse_t& ell2);

double gaussian_wasserstein_2d(const Ellipse_t& ell1, const Ellipse_t& ell2);

double bhattacharyya_distance(const Ellipse_t& ell1, const Ellipse_t& ell2);

double generalized_iou_(const Ellipse_t& ell1, const Ellipse_t& ell2);

#endif // METRICS_H
