/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <functional>
#include <future>
#include <queue>
#include <thread>
#include <vector>


class ThreadPool
{
public:
  ThreadPool(size_t nbThreads);

  /// Enqueue a new task
  template<class F, class... Args>
  auto enqueue(F&& f, Args&&... args)
    -> std::future<typename std::result_of<F(Args...)>::type>;

  ~ThreadPool();


private:
  std::vector<std::thread> workers;

  std::queue< std::function<void()> > tasks;

  std::mutex queue_mutex;
  std::condition_variable condition;
  std::atomic<bool> stop;
};



template<class F, class... Args>
auto ThreadPool::enqueue(F&& f, Args&&... args)
  -> std::future<typename std::result_of<F(Args...)>::type>
{
  using return_type = typename std::result_of<F(Args...)>::type;

  // Create a new task
  auto task = std::make_shared< std::packaged_task<return_type()> >(
        std::bind(std::forward<F>(f), std::forward<Args>(args)...)
  );
  std::future<return_type> res = task->get_future();
  {
    // lock other threads
    std::unique_lock<std::mutex> lock(queue_mutex);

    if (stop)
      throw std::runtime_error("Enqueue on stoppped Thread Pool");

    // enqueue the task in the list of tasks
    tasks.emplace([task](){ (*task)(); });
  }

  condition.notify_one();
  return res;
}

#endif // THREADPOOL_H
