/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ee.h"

#include <array>
#include <cmath>
#include <iostream>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/foreach.hpp>
#include <boost/geometry/geometries/adapted/c_array.hpp>

typedef boost::geometry::model::d2::point_xy<double, boost::geometry::cs::cartesian> point_2d;
typedef boost::geometry::model::polygon<point_2d> polygon_2d;

using namespace boost::geometry;


// ----------------------------------------------------------------------------
// Different methods for computing the Intersection-over-Union between ellipses.
//  - The method called "toms" seems the best one (faster and more accurate)
//    compared to "gsl" or "gems".
//  - The "boost" method is a safe approximate method in case of failure of "toms".
// ----------------------------------------------------------------------------


double compute_iou_toms(const Ellipse_t& ell0, const Ellipse_t& ell1)
{
    double x[4], y[4];
    int n_roots;
    int ret;
    // first try with TOMS
    double inter = ellipse_ellipse_overlap_netlibs(ell0.angle, ell0.axes[0], ell0.axes[1], ell0.center[0], ell0.center[1],
                                                   ell1.angle, ell1.axes[0], ell1.axes[1], ell1.center[0], ell1.center[1],
                                                   x, y, &n_roots, &ret);
    double area0 = ell0.axes[0] * ell0.axes[1] * M_PI;
    double area1 = ell1.axes[0] * ell1.axes[1] * M_PI;
    if (inter < 0 || inter * 2 > area0 + area1)
    {
        inter = compute_overlap_boost(ell0, ell1);
    }
    return inter / (area0 + area1 - inter);
}



double compute_iou_toms(const Ellipse& ell0, const Ellipse& ell1)
{
    double x[4], y[4];
    int n_roots;
    int ret;
    // first try with TOMS
    double inter = ellipse_ellipse_overlap_netlibs(ell0[2], ell0[0], ell0[1], ell0[3], ell0[4],
                                                   ell1[2], ell1[0], ell1[1], ell1[3], ell1[4],
                                                   x, y, &n_roots, &ret);
    double area0 = ell0[0]*ell0[1]*M_PI;
    double area1 = ell1[0]*ell1[1]*M_PI;
    if (inter < 0 || inter*2 > area0 + area1)
    {
        inter = compute_overlap_boost(ell0, ell1);
    }
    return inter / (area0 + area1 - inter);
}





double compute_iou_gsl(const Ellipse& ell0, const Ellipse& ell1, int choice)
{
    double x[4], y[4];
    int n_roots;
    int ret;
    // first try with GSL
    double inter = ellipse_ellipse_overlap_gsl(ell0[2], ell0[0], ell0[1], ell0[3], ell0[4],
                                               ell1[2], ell1[0], ell1[1], ell1[3], ell1[4],
                                               x, y, &n_roots, &ret, choice);
    double area0 = ell0[0]*ell0[1]*M_PI;
    double area1 = ell1[0]*ell1[1]*M_PI;
    if (inter < 0 || inter*2 > area0 + area1)
    {
        inter = compute_overlap_boost(ell0, ell1);
    }
    return inter / (area0 + area1 - inter);
}


double compute_iou_gems(const Ellipse& ell0, const Ellipse& ell1)
{
    double x[4], y[4];
    int n_roots;
    int ret;
    // first try with GEMS
    double inter = ellipse_ellipse_overlap_gems(ell0[2], ell0[0], ell0[1], ell0[3], ell0[4],
                                                ell1[2], ell1[0], ell1[1], ell1[3], ell1[4],
                                                x, y, &n_roots, &ret);
    double area0 = ell0[0]*ell0[1]*M_PI;
    double area1 = ell1[0]*ell1[1]*M_PI;
    if (inter < 0 || inter*2 > area0 + area1)
    {
        inter = compute_overlap_boost(ell0, ell1);
    }
    return inter / (area0 + area1 - inter);
}





int ellipse2poly(float PHI_1, float A1, float B1, float H1, float K1, polygon_2d * po)
{
    polygon_2d  poly;
    float xc = H1;
    float yc = K1;
    float a = A1;
    float b = B1;
    float w = PHI_1;
    
    const int N = 20; // Default discretization
    float t = 0;
    int i = 0;
    double coor[N*2+1][2];
  
    double x, y;
    float step = M_PI/N;
    float sinphi = sin(w);
    float cosphi = cos(w);
    for(i=0; i<2*N+1; i++)
    {   
        x = xc + a*cos(t)*cosphi - b*sin(t)*sinphi;
        y = yc + a*cos(t)*sinphi + b*sin(t)*cosphi;
        if(fabs(x) < 1e-4) x = 0;
        if(fabs(y) < 1e-4) y = 0;

        coor[i][0] = x;
        coor[i][1] = y;
        t += step;
    }

    assign_points(poly, coor);
    correct(poly);
    *po = poly;
    return 1;
}

// overlaping area of two polygons
float getOverlapingAreaPoly(polygon_2d poly, polygon_2d poly2)
{
    float overAreaPoly = 0.0;    
    std::deque<polygon_2d> output;
    bool ret = boost::geometry::intersection(poly, poly2, output);
    if(!ret) {
        std::cout << "Could not calculate the overlap of the polygons\n";
        exit(-1);
    }
    BOOST_FOREACH(polygon_2d const& p, output)
    {
        overAreaPoly = boost::geometry::area(p);
    }
    return overAreaPoly;
}


double compute_iou_boost(const Ellipse& ell0, const Ellipse& ell1)
{
    double area0 = ell0[0]*ell0[1]*M_PI;
    double area1 = ell1[0]*ell1[1]*M_PI;
    double inter = compute_overlap_boost(ell0, ell1);
    return inter / (area0 + area1 - inter);
}


double compute_overlap_boost(const Ellipse& ell0, const Ellipse& ell1)
{
    polygon_2d poly0, poly1;
    ellipse2poly(ell0[2], ell0[0], ell0[1], ell0[3], ell0[4], &poly0);
    ellipse2poly(ell1[2], ell1[0], ell1[1], ell1[3], ell1[4], &poly1);
    return getOverlapingAreaPoly(poly0, poly1);
}

double compute_overlap_boost(const Ellipse_t& ell0, const Ellipse_t& ell1)
{
    polygon_2d poly0, poly1;
    ellipse2poly(ell0.angle, ell0.axes[0], ell0.axes[1], ell0.center[0], ell0.center[1], &poly0);
    ellipse2poly(ell1.angle, ell1.axes[0], ell1.axes[1], ell1.center[0], ell1.center[1], &poly1);
    return getOverlapingAreaPoly(poly0, poly1);
}

