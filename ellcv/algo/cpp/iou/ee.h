/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EE_H
#define EE_H

#include "solvers.h"
#include "types.h"

using Ellipse = std::array<double, 5>;

double compute_iou_toms(const Ellipse_t& ell0, const Ellipse_t& ell1);
double compute_iou_toms(const Ellipse& ell0, const Ellipse& ell1);

double compute_iou_gsl(const Ellipse& ell0, const Ellipse& ell1, int choice);

double compute_iou_gems(const Ellipse& ell0, const Ellipse& ell1);

double compute_iou_boost(const Ellipse& ell0, const Ellipse& ell1);
double compute_overlap_boost(const Ellipse_t& ell0, const Ellipse_t& ell1);
double compute_overlap_boost(const Ellipse& ell0, const Ellipse& ell1);


#endif // EE_H
