/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TYPES_H
#define TYPES_H

#include <vector>
#include <Eigen/Dense>

struct Ellipse_t
{
    Eigen::Vector2d axes;
    Eigen::Vector2d center;
    double angle;
    
    public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

struct Ellipsoid_t
{
    Eigen::Vector3d axes;
    Eigen::Vector3d center;
    Eigen::Matrix3d orientation;    
};

using Ellipsoid_m = Eigen::Matrix4d;
using Ellipsoid_m_list = std::vector<Ellipsoid_m, Eigen::aligned_allocator<Ellipsoid_m>>;

using Ellipsoid_e = Eigen::Matrix3d;
using Ellipsoid_e_list = std::vector<Ellipsoid_e>;

using Ellipsoid_t_list = std::vector<Ellipsoid_t>;

using Ellipse_m = Eigen::Matrix3d;
using Ellipse_m_list = std::vector<Ellipse_m, Eigen::aligned_allocator<Ellipse_m>>;
using Ellipse_m_list_of_list = std::vector<Ellipse_m_list>;

using Ellipse_t_list = std::vector<Ellipse_t>;
using Ellipse_t_list_of_list = std::vector<Ellipse_t_list>;

using Pose_m = Eigen::Matrix<double, 3, 4>;
using Pose_m_list = std::vector<Pose_m, Eigen::aligned_allocator<Pose_m>>;

using Bbox2 = Eigen::Matrix2d;
using Bbox2_list = std::vector<Bbox2, Eigen::aligned_allocator<Bbox2>>;
using Bbox2_list_of_list = std::vector<Bbox2_list>;

using Mapping = std::vector<std::vector<int>>;

using Cone_m = Eigen::Matrix3d;
using Cone_m_list = std::vector<Cone_m>;
using Cone_m_list_of_list = std::vector<Cone_m_list>;

using Cone_axis = Eigen::Vector3d;
using Cone_axis_list = std::vector<Cone_axis>;
using Cone_axis_list_of_list = std::vector<Cone_axis_list>;

using Projection_m = Eigen::Matrix<double, 3, 4>;

using Point2d = Eigen::Vector2d;
using Point2d_list = std::vector<Point2d, Eigen::aligned_allocator<Point2d>>;
using Point2d_list_of_list = std::vector<Point2d_list>;

#endif // TYPES_H
