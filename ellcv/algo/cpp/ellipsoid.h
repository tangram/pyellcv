/**
 * This file is part of PyEllCV.
 *
 * Author: Matthieu Zins (matthieu.zins@inria.fr)
 *
 * PyEllCV is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PyEllCV is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ELLIPSOID_H
#define ELLIPSOID_H

#include <Eigen/Dense>

#include "types.h"

Ellipsoid_t decompose_ellipsoid(const Ellipsoid_m& Q);

Ellipsoid_t_list decompose_ellipsoids(const Ellipsoid_m_list& ellipsoids);

std::vector<Eigen::Matrix3d> compute_ellipsoids_euclidean_forms(const Ellipsoid_t_list& ellipsoids);

Ellipse_t_list project_ellipsoids(const Ellipsoid_m_list& ellipsoids, const Projection_m& P);
#endif // ELLIPSE_H
