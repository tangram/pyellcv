# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

from ellcv.types import Ellipsoid, Ellipse
from ellcv.utils import sym2vec, vec2sym

import numpy as np


def compute_B_perspective(P):
    p11, p12, p13, p14 = P[0, :]
    p21, p22, p23, p24 = P[1, :]
    p31, p32, p33, p34 = P[2, :]
    B = np.array([[p11**2, 2*p12*p11, 2*p13*p11, 2*p14*p11, p12**2, 2*p13*p12, 2*p14*p12, p13**2, 2*p13*p14, p14**2],
                  [p21*p11, p21*p12+p22*p11, p23*p11+p21*p13, p24*p11+p21*p14, p22*p12, p22*p13+p23*p12, p22*p14+p24*p12, p23*p13, p23*p14+p24*p13, p24*p14],
                  [p31*p11, p31*p12+p32*p11, p33*p11+p31*p13, p34*p11+p31*p14, p32*p12, p32*p13+p33*p12, p32*p14+p34*p12, p33*p13, p33*p14+p34*p13, p34*p14],
                  [p21**2, 2*p22*p21, 2*p23*p21, 2*p24*p21, p22**2, 2*p23*p22, 2*p24*p22, p23**2, 2*p23*p24, p24**2],
                  [p31*p21, p31*p22+p32*p21, p33*p21+p31*p23, p34*p21+p31*p24, p32*p22, p32*p23+p33*p22, p32*p24+p34*p22, p33*p23, p33*p24+p34*p23, p34*p24],
                  [p31**2, 2*p32*p31, 2*p33*p31, 2*p34*p31, p32**2, 2*p33*p32, 2*p34*p32, p33**2, 2*p33*p34, p34**2]])
    return B



"""
    For orthogonal projection, P has the following form:
    P = [[p11 p12 p13 p14]
         [p21 p22 p23 p24]
         [ 0   0   0   1 ]]
"""
def compute_B_ortho(P):
    p11, p12, p13, p14 = P[0, :]
    p21, p22, p23, p24 = P[1, :]
    p31, p32, p33, p34 = P[2, :]
    B = np.array([[p11**2, 2*p12*p11, 2*p13*p11, 2*p14*p11, p12**2, 2*p13*p12, 2*p14*p12, p13**2, 2*p13*p14, p14**2],
                  [p21*p11, p21*p12+p22*p11, p23*p11+p21*p13, p24*p11+p21*p14, p22*p12, p22*p13+p23*p12, p22*p14+p24*p12, p23*p13, p23*p14+p24*p13, p24*p14],
                  [0, 0, 0, p11, 0, 0, p12, 0, p13, p14],
                  [p21**2, 2*p22*p21, 2*p23*p21, 2*p24*p21, p22**2, 2*p23*p22, 2*p24*p22, p23**2, 2*p23*p24, p24**2],
                  [0, 0, 0, p21, 0, 0, p22, 0, p23, p24],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]])
    return B



def reconstruct_ellipsoid_(ellipses, projections, ortho_proj=False):
    n_views = len(ellipses)
    M = np.zeros((6 * n_views, 10 + n_views))
    for i, ell in enumerate(ellipses):
        ax, _, center = ell.decompose()
        C = ell.as_dual()
        h = np.sqrt(np.sum(ax**2))
        H = np.diag([h, h, 1.0])
        H[:2, 2] = center
        H_inv = np.linalg.inv(H)

        P = projections[i]
        new_P = P.T @ H_inv.T

        if not ortho_proj:
            B = compute_B_perspective(new_P.T)
        else:
            B = compute_B_ortho(new_P.T)

        # normalize and center ellipse
        C_nc = H_inv @ C @ H_inv.T
        C_nc_vec = sym2vec(C_nc)
        C_nc_vec /= -C_nc_vec[-1]

        M[6*i:6*i+6, :10] = B
        M[6*i:6*i+6, 10+i] = -C_nc_vec

    _, _, Vt = np.linalg.svd(M)
    w = Vt[-1, :]
    Qadj_vec = w[:10]
    Qadj = vec2sym(Qadj_vec)
    return Qadj


def reconstruct_ellipsoid(ellipses, projections, use_two_passes=True, ortho_proj=False):
    # first pass
    Q = reconstruct_ellipsoid_(ellipses, projections, ortho_proj)   # initial estimate

    if use_two_passes:
        # optional second pass with better pre-conditioning by removing
        # the estimated translation
        _, _, center_est = Ellipsoid(Q).decompose(no_warnings=True)
        T = np.eye(4)
        T[:3, 3] = -center_est
        new_projections = [P @ T for P in projections]

        Q = reconstruct_ellipsoid_(ellipses, new_projections, ortho_proj)

        # remove translation
        Q = T @ Q @ T.T
        Q = 0.5 *(Q + Q.T)

    # TODO: fix reconstruction gives negative eigen values. Temorarely fixed
    # by decomposition and recomposition
    axes, R, center = Ellipsoid.from_dual(Q).decompose(no_warnings=True)
    return Ellipsoid.compose(axes, R, center)

