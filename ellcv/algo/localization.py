# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np


def compute_position_from_2d3d(orientation, pts_2d, pts_3d, K_inv=None):
    """
    Linear solution for computing the camera position from 2d-3d points correspondences and known orientation
    (requires at least 2 correspondences)

    Parameters
    ----------
    R : orientation.
    pts_2d : [N, 2] either pixel coords and K is defined or normalized camera coordinates
    pts_3d : [N, 3] 3d points
    K_inv : inverse matrix of the camera intrinsics (required if pts_2d are pixels)

    Returns
    -------
    camera position

    """

    R = orientation.T

    if K_inv is not None:
        pts_2d_h = np.hstack((pts_2d, np.ones((pts_2d.shape[0], 1)))).T
        pts_2d_h_n = K_inv @ pts_2d_h
        pts_2d_n = pts_2d_h_n[:2, :].T
    else:
        pts_2d_n = pts_2d

    A = []
    b = []
    for uv, X in zip(pts_2d_n, pts_3d):
        x = uv[0]
        y = uv[1]
        RX = R @ X
        A.append([0, -1, y])
        b.append([RX[1] - y * RX[2]])
        A.append([1, 0, -x])
        b.append([x * RX[2] - RX[0]])
    A = np.vstack(A)
    b = np.vstack(b)

    # t_est = np.linalg.inv(A.T @ A) @ A.T @ b
    # Use of SVD is slower but more accurate when A is ill-conditionned
    U, S, Vt = np.linalg.svd(A, full_matrices=False)
    t_est = Vt.T @ np.diag(np.reciprocal(S)) @ U.T @ b.reshape((-1, 1))

    t_est = t_est.flatten()

    position_est = -orientation @ t_est

    return position_est



