# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from ellcv.types import Ellipsoid, Ellipse
from ellcv.visu import sample_ellipse_points


def intersection_plane_line(plane, line):
    """
        Parameters:
            - plane: in the form ax + by + cz + d = 0 (with d=-delta)
            - direction: in the form [direction, point]
    """
    n = plane[:3]
    delta = -plane[3]
    c = line[1]
    l = (delta - n.dot(c)) / n.dot(line[0])
    return c + line[0] * l


def ellipsoid_polar_plane(ellipsoid, camera_position, vertical=[0.0, 0.0, 1.0]):
    """
        Return the polar plane coordinate system. The polar plane is at the 
        intersection between an ellipsoid and the tangent view cone.
        Parameters:
            - ellipsoid: ellisoid in world coordinates
            - camera_position
            - vertical: this vector is used to define the y-axis of the plane
        Returns:
            - plane orientation
            - plane position
    """
    # Equation of the plane from Multiple View Geometry, Hartley & Zissermann.
    C = np.hstack((camera_position, [1.0]))
    Q = ellipsoid.as_primal()
    plane = Q @ C
    plane /= np.linalg.norm(plane[:3])
    
    # Define a plane coordinates system
    n = plane[:3]
    # z = np.array([0.0, 0.0, 1.0]) #TODO choose another vector if z and view direction are not collinear
    z = np.asarray(vertical)
    v = z - z.dot(n) * n
    v /= np.linalg.norm(v)
    u = np.cross(v, n)
    u /= np.linalg.norm(u)
    orientation = np.vstack((u, v, n)).T

    # Define origin as intersection of the ray camera-ellipsoid and the plane
    origin = intersection_plane_line(plane, [ellipsoid.center - camera_position,
                                            camera_position])
    return orientation, origin

    
def ellipsoid_plane_intersection(ellipsoid, plane):
    """
        Find analytically the ellipse formed by the intersection of an ellipsoid
        with a plane.
        Parameters:
            - ellipsoid
            - plane: [orientation, position] of the plane coord. system
        Return:
            - ellipse (2D in the plane coord. system)
    """
    plane_orientation = plane[0]
    plane_position = plane[1]
    
    # Transform from world to plane
    R_to_plane = plane_orientation.T
    t_to_plane = -plane_orientation.T @ plane_position
    
    # Transform ellipsoid in plane coord. system
    ellipsoid_plane = ellipsoid.transform(R_to_plane, t_to_plane)

    # Get the ellipse at the intersection between the ellipsoid and the plane (z=0)
    A = ellipsoid_plane.as_primal()
    C = np.delete(np.delete(A, 2, axis=0), 2, axis=1)  
    return Ellipse.from_primal(C)
    
    
    
    
def fit_ellipseAC(pts):
    """
        Fit an ellipse to 2D points using the normalization A+C=1
    """
    A = []
    b = []
    for pt in pts:
        A.append([pt[0]**2-pt[1]**2, pt[0]*pt[1], pt[0], pt[1], 1])
        b.append(-pt[1]**2)
    A = np.vstack(A)
    b = np.asarray(b)
    est = np.linalg.inv(A.T @ A) @ A.T @ b
    C = np.array([[est[0], est[1]/2, est[2]/2],
                  [est[1]/2, 1.0 - est[0], est[3]/2],
                  [est[2]/2, est[3]/2, est[4]]])
    return Ellipse.from_primal(C)



def cone_plane_intersection(cone, plane):
    """
        Compute the ellipse at the intersection between a cone and a plane.
        Parameters:
            - cone: sampled cone [directions, position]
            - plane: [orientation, position]
    """
    plane_orientation, plane_position = plane
    # convert to plane equation: ax+by+cz+d=0 (with d=-delta)
    plane_normal = plane_orientation[:, 2]
    delta = plane_normal.dot(plane_position)
    plane_equation = np.asarray(plane_normal.tolist() + [-delta])


    directions = cone[0]
    center = cone[1]
    points = []
    for dir in directions:
        pt = intersection_plane_line(plane_equation, [dir, center])
        points.append(pt)
    points = np.vstack(points)

    # transform to plane coordinates
    R = plane_orientation.T
    t = -R @ plane_position
    points_plane = (R @ points.T + t.reshape((-1, 1))).T
    ellipse_plane = fit_ellipseAC(points_plane[:, :2])
    return ellipse_plane
