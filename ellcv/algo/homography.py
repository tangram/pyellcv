from ellcv.types import  Ellipse
import itertools

import numpy as np


######## Homography Estimation from conics (ellipses) ########
#
# Rerefence: http://www.macs.hw.ac.uk/bmvc2006/papers/306.pdf
#
###


def estimate_H_from_ellipses_3_pairs_and_more(ellipses_init, ellipses_transf):
    """
        Estimate an homography from 3 pairs of ellipses and more.
        Inputs:
            - ellipses_init: list of initial ellipses
            - ellipses_transf: list of transformed ellipses
        Returns:
            - H: (3x3) homography matrix
    """
    Cs = [ell.as_primal() for ell in ellipses_init]
    Cs_ = [ell.as_primal() for ell in ellipses_transf]

    for C in Cs:
        C /= np.linalg.norm(C)
    for C in Cs_:
        C /= np.linalg.norm(C)
    for C, C_ in zip(Cs, Cs_):
        s = pow(np.linalg.det(C_) / np.linalg.det(C), 1.0/3)
        C *= s

    M = []
    for i in range(len(Cs)):
        for j in range(i+1, len(Cs_)):
            A = np.linalg.inv(Cs_[i]) @ Cs_[j]
            B = np.linalg.inv(Cs[i]) @ Cs[j]
            MM_A = np.array([
                [A[0, 0], 0.0, 0.0, A[0, 1], 0.0, 0.0, A[0, 2], 0.0, 0.0],
                [0.0, A[0, 0], 0.0, 0.0, A[0, 1], 0.0, 0.0, A[0, 2], 0.0],
                [0.0, 0.0, A[0, 0], 0.0, 0.0, A[0, 1], 0.0, 0.0, A[0, 2]],

                [A[1, 0], 0.0, 0.0, A[1, 1], 0.0, 0.0, A[1, 2], 0.0, 0.0],
                [0.0, A[1, 0], 0.0, 0.0, A[1, 1], 0.0, 0.0, A[1, 2], 0.0],
                [0.0, 0.0, A[1, 0], 0.0, 0.0, A[1, 1], 0.0, 0.0, A[1, 2]],

                [A[2, 0], 0.0, 0.0, A[2, 1], 0.0, 0.0, A[2, 2], 0.0, 0.0],
                [0.0, A[2, 0], 0.0, 0.0, A[2, 1], 0.0, 0.0, A[2, 2], 0.0],
                [0.0, 0.0, A[2, 0], 0.0, 0.0, A[2, 1], 0.0, 0.0, A[2, 2]]
                ]
            )

            MM_B = np.array([
                [-B[0, 0], -B[1, 0], -B[2, 0], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [-B[0, 1], -B[1, 1], -B[2, 1], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [-B[0, 2], -B[1, 2], -B[2, 2], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, -B[0, 0], -B[1, 0], -B[2, 0], 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, -B[0, 1], -B[1, 1], -B[2, 1], 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, -B[0, 2], -B[1, 2], -B[2, 2], 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -B[0, 0], -B[1, 0], -B[2, 0]],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -B[0, 1], -B[1, 1], -B[2, 1]],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -B[0, 2], -B[1, 2], -B[2, 2]]
                ]
            )
            MM = MM_A +MM_B
            M.append(MM)


    M = np.vstack(M)
    U, S, Vt = np.linalg.svd(M)
    H_est = Vt[-1, :]
    H_est = H_est.reshape((3, 3))
    H_est /= H_est[2, 2]
    return H_est

def estimate_H_from_ellipses_2_pairs(ellipses_init, ellipses_transf):
    """
        Estimate an homography from only two pairs of ellipses. (more experimental)
        Inputs:
            - ellipses_init: list of initial ellipses (only uses the first two)
            - ellipses_transf: list of transformed ellipses (only uses the first two)
        Returns:
            list of solutions: [(error1, H1), (error2, H2), ...]
            sorted by increasing error
    """
    Cs = [ell.as_primal() for ell in ellipses_init]
    Cs_ = [ell.as_primal() for ell in ellipses_transf]

    for C in Cs:
        C /= np.linalg.norm(C)
    for C in Cs_:
        C /= np.linalg.norm(C)
    for C, C_ in zip(Cs, Cs_):
        s = pow(np.linalg.det(C_) / np.linalg.det(C), 1.0/3)
        C *= s

    C1 = Cs[0]
    C1_ = Cs_[0]

    C2 = Cs[1]
    C2_ = Cs_[1]


    V1, D1, Vt1 = np.linalg.svd(C1)
    D1 = np.diag(D1)
    V1_, D1_, Vt1_ = np.linalg.svd(C1_)
    D1_ = np.diag(D1_)

    F1 = V1 @ np.sqrt(D1)
    F1_ = V1_ @ np.sqrt(D1_)


    A = np.linalg.inv(F1_) @ C2_ @ np.linalg.inv(F1_).T
    B = np.linalg.inv(F1) @ C2 @ np.linalg.inv(F1).T

    DA, Q = np.linalg.eig(A)
    # DA = np.diag(DA)
    DB, U = np.linalg.eig(B)
    # DB = np.diag(DB)

    # find the best permutations of B's eigenvalues
    permutations = []
    for permut in itertools.permutations(range(3)):
        DB2 = [DB[i] for i in permut]
        diff = np.sum(np.abs(DA - DB2)**2)
        permutations.append([diff, permut])
    permutations = sorted(permutations)
    # re-order the eigen vectors
    U = U[:, permutations[0][1]]

    # one of these solutions should be correct
    P1 = np.diag([1, 1, 1])
    P2 = np.diag([-1, 1, 1])
    P3 = np.diag([1, -1, 1])
    P4 = np.diag([1, 1, -1])
    solutions = []
    for P in [P1, P2, P3, P4]:
        H = np.linalg.inv(F1_).T @ Q @ P @ U.T @ F1.T
        check = np.sum((np.linalg.inv(C1_) @ C2_ @ H - H @ np.linalg.inv(C1) @ C2)**2)
        solutions.append([check, H])
    solutions = sorted(solutions)
    return solutions