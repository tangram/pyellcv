# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as patches

def draw_ellipse_dashed(image, ellipse, color=(0, 255, 0), thickness=1, dash_space=10, dash_size=3):
    axes, angle, center = ellipse.decompose()
    center = np.round(center).astype(int).clip(-10000, 10000)
    axes = np.round(axes).astype(int).clip(-10000, 10000)
    angle = round(np.rad2deg(angle))
    for i in range(0, 360, dash_space):
        cv2.ellipse(image, tuple(center), tuple(axes), angle,
                    i, i+dash_size, color, thickness)
    return image

def draw_ellipse(image, ellipse, color=(0, 255, 0), thickness=1):
    axes, angle, center = ellipse.decompose()
    center = np.round(center).astype(int).clip(-10000, 10000)
    axes = np.round(axes).astype(int).clip(-10000, 10000)
    angle = round(np.rad2deg(angle))
    cv2.ellipse(image, tuple(center), tuple(axes), angle,
                0, 360, color, thickness)
    return image

def draw_bbox(image, bbox, color=(0, 255, 0), thickness=1):
    x0, y0, x1, y1 = map(int, map(round, bbox))
    cv2.rectangle(image, (x0, y0), (x1, y1), color, thickness)
    return image

def draw_bbox_plt(bbox, color="green", thickness=1):
    w = bbox[2] - bbox[0]
    h = bbox[3] - bbox[1]
    rect = patches.Rectangle(bbox[:2], w, h, linewidth=thickness, edgecolor=color, facecolor='none')
    ax = plt.gca()
    ax.add_patch(rect)

def sample_ellipse_points_params(axes, angle, center, sampling=360):
    """
        Sample 2D points from ellipse parameters.
    """
    t = np.linspace(0, 2*np.pi, sampling, endpoint=False)
    xy = np.vstack([axes[0] * np.cos(t), axes[1] * np.sin(t)])
    R = np.array([[np.cos(angle), -np.sin(angle)],
                  [np.sin(angle), np.cos(angle)]])
    xy = R @ xy + center.reshape((-1, 1))
    return xy.T

def sample_ellipse_points(ellipse, sampling=360):
    """
        Sample 2D points on the ellipse.
    """
    axes, angle, center = ellipse.decompose()
    return sample_ellipse_points_params(axes, angle, center, sampling)


def draw_ellipse_plt(ellipse, size=1, color="green", sampling=360, limits=None):
    """
        Draw an ellipse in Matplotlib.
    """
    pts = sample_ellipse_points(ellipse, sampling=sampling)
    plt.scatter(pts[:, 0], pts[:, 1], size, color)
    if limits is not None:
        plt.xlim(limits[0])
        plt.ylim(limits[1])

def draw_points(image, pts, color=(0, 255, 0), size=1):
    good = np.where(np.logical_and(np.logical_and(pts[:, 0] >= 0, pts[:, 0] < image.shape[1]),
                                   np.logical_and(pts[:, 1] >= 0, pts[:, 1] < image.shape[0])))[0]
    for pt in np.round(pts[good, :]).astype(int):
        cv2.circle(image, (pt[0], pt[1]), size, color, thickness=-1)
    return image
