# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np


def generate_uv_sphere_pointcloud_(azimuths, elevations, sampling=100):
    all_pts = []
    for az in np.linspace(0.0, np.deg2rad(360.0), azimuths+ 1, endpoint=True):
        elev = np.linspace(-np.deg2rad(90.0), np.deg2rad(90.0), sampling, endpoint=True)
        x = np.cos(az) * np.cos(elev)
        z = np.sin(az) * np.cos(elev)
        y = np.sin(elev)   
        pts = np.vstack((x, y, z)).T
        all_pts.append(pts)
    
    for elev in np.linspace(-np.deg2rad(90.0), np.deg2rad(90.0), elevations, endpoint=True):
        az = np.linspace(0.0, np.deg2rad(360.0), sampling, endpoint=True)
        x = np.cos(az) * np.cos(elev)
        z = np.sin(az) * np.cos(elev)
        y = np.sin([elev] * len(x)) 
        pts = np.vstack((x, y, z)).T
        all_pts.append(pts)
    
    return np.vstack(all_pts)

def generate_ellipsoid_pointcloud(ellipsoid, resolution_az=10, resolution_elev=10, sampling=100):
    pts = generate_uv_sphere_pointcloud_(resolution_az, resolution_elev, sampling)

    axes, R, center = ellipsoid.decompose()
    pts[:, 0] *= axes[0]
    pts[:, 1] *= axes[1]
    pts[:, 2] *= axes[2]
    pts = (R @ pts.T).T
    pts += center
    return pts

def generate_triaxe_pointcloud(pose, size=1.0, sampling=10):
    pts = np.zeros((sampling * 3, 3), dtype=float)
    pts[:sampling, 0] = np.linspace(0.0, size, sampling)
    pts[sampling:2*sampling, 1] = np.linspace(0.0, size, sampling)
    pts[2*sampling:, 2] = np.linspace(0.0, size, sampling)
    pts = (pose[0] @ pts.T + pose[1].reshape((-1, 1))).T
    colors = np.zeros((sampling * 3, 3), dtype=np.uint8)
    colors[:sampling, 0] = 255
    colors[sampling:2*sampling, 1] = 255
    colors[2*sampling:, 2] = 255
    if sampling == 1:
        pts = pts[:1, :]
        colors = colors[:1, :]
    return pts, colors


def generate_camera_mesh(pose, dx, dy, dz, color=(0, 0, 0), with_triaxe=False, triaxe_scale=0.25):
    pts = np.array([
        [0.0, 0.0, 0.0],
        [-0.50, -0.5, 1.0],
        [ 0.50, -0.5, 1.0],
        [ 0.50,  0.5, 1.0],
        [-0.50,  0.5, 1.0],
    ])
    lines = [[0, 1], [0, 2], [0, 3], [0, 4],
             [1, 2], [2, 3], [3, 4], [4, 1]]

    cols = np.zeros((5, 3), dtype=np.uint8)
    for i in range(3): cols[:, i] = color[i]
    if with_triaxe:
        tri_pts, tri_cols = generate_triaxe_pointcloud([np.eye(3), np.zeros(3)], triaxe_scale, sampling=50)
        pts = np.vstack((pts, tri_pts))
        cols = np.vstack((cols, tri_cols))
    pts[:5, 0] *= dx
    pts[:5, 1] *= dy
    pts[:5, 2] *= dz
    pts[5:] *= dz

    pts = (pose[0] @ pts.T + pose[1].reshape((-1, 1))).T
    return pts, cols, lines


def draw_bbox3d_OBJ(filename, bb):
    x1, y1, z1, x2, y2, z2 = bb
    corners = np.array([
    [x1, y1, z1],
    [x1, y2, z1],
    [x2, y2, z1],
    [x2, y1, z1],
    [x1, y1, z2],
    [x1, y2, z2],
    [x2, y2, z2],
    [x2, y1, z2]])
    edges = np.array([[0, 1], [1, 2], [2, 3], [3, 0], [4, 5], [5, 6], [6, 7], [7, 4], [0, 4], [1, 5], [2, 6], [3, 7]])
    with open(filename, "w") as fout:
        for v in corners:
            fout.write("v %f %f %f\n" % (v[0], v[1], v[2]))
        for e in edges:
            fout.write("l %d %d\n" % (e[0]+1, e[1]+1))
