# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

def write_ply(filename, pts, colors=None):
    """
        Write a PLY file from a pointcloud (Nx3) and optional colors (Nx3)
    """
    assert pts.shape[1] == 3
    ply_header = """ply
format ascii 1.0
comment VCGLIB generated
element vertex %d
property float x
property float y
property float z
property uchar red
property uchar green
property uchar blue
element face 0
property list uchar int vertex_indices
end_header\n""" % pts.shape[0]
    if colors is None:
        ply_header = """ply
format ascii 1.0
comment VCGLIB generated
element vertex %d
property float x
property float y
property float z
element face 0
property list uchar int vertex_indices
end_header\n""" % pts.shape[0]
    with open(filename, "w") as fout:
        fout.write(ply_header)
        for i, p in enumerate(pts):
            s = " ".join(map(str, p))
            if colors is not None:
                s += " " + " ".join(map(str, colors[i]))
            s += "\n"
            fout.write(s)