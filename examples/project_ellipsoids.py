# This file is part of PyEllCV.
#
# Author: Matthieu Zins (matthieu.zins@inria.fr)
#
# PyEllCV is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PyEllCV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PyEllCV.  If not, see <http://www.gnu.org/licenses/>.

import cv2
import numpy as np
from ellcv.types import Ellipsoid
from ellcv.utils import generate_random_scene, generate_random_camera, \
                        generate_K, bbox_from_ellipse
from ellcv.visu import draw_ellipse, draw_bbox


# Create a scene of ellipsoids
N = 4
scene = generate_random_scene(N, axes_range=[0.3, 1.5], position_range=[-3.0, 3.0])

# Define the camera
W, H = 640, 480
o, p = generate_random_camera(dist_range=[10.0, 15.0], target_range=[-0.5, 0.5])
K = generate_K(500, W/2, H/2)
P = K @ np.hstack((o.T, -o.T@p.reshape((3, 1))))

# Project and display
img = np.zeros((H, W, 3), dtype=np.uint8)
for ell in scene:
    ellipse = ell.project(P)
    bbox = bbox_from_ellipse(ellipse)
    draw_bbox(img, bbox, color=(255, 0, 0))
    draw_ellipse(img, ellipse, color=(0, 255, 0))
cv2.imshow("viz", img)
cv2.waitKey()